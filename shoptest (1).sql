-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2024 at 08:15 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoptest`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `row` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `fname`, `lname`, `nickname`, `row`) VALUES
(1, 'sksmaster123', '4a094858b33da6d33ff6bdb99ea4034c', 'Sukasa', 'Saeheng', 'Sing', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `namebrand` varchar(255) NOT NULL,
  `row` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `namebrand`, `row`) VALUES
(1, 'uniqlo', 1),
(2, 'nike', 1),
(3, 'H&M', 1),
(4, 'adidas', 1),
(5, 'New_Balance', 0),
(6, 'Lee', 1),
(7, 'Mc_Jeans', 1),
(8, 'AIIZ', 1),
(9, 'LEVI', 1),
(12, 'Lacoste', 0);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `sub_district` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `postcode` int(6) NOT NULL,
  `row` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `username`, `password`, `name`, `lname`, `nickname`, `tel`, `address`, `sub_district`, `district`, `province`, `postcode`, `row`) VALUES
(1, 'sksmaster123', '4a094858b33da6d33ff6bdb99ea4034c', 'Sukasa', 'Sae-heng', 'Sing', '0808867088', '', '', '', '', 0, 'member'),
(3, 'bstopp1', 'pW3!PaY#', 'Bernardina', 'Stopp', '', '0808867088', '', '', '', '', 0, 'member'),
(4, 'frigeby2', 'oV0.kFWP%R!', 'Fletcher', 'Rigeby', '', '0', '', '', '', '', 0, 'member'),
(5, 'kcully3', 'hA4!|5$F4A_y2n', 'Karole', 'Cully', '', '0', '', '', '', '', 0, 'member'),
(6, 'cmelato4', 'gR2$vARWnK', 'Carline', 'Melato', '', '0', '', '', '', '', 0, 'member'),
(7, 'paberchirder5', 'kM0!11,cM3o', 'Phebe', 'Aberchirder', '', '0', '', '', '', '', 0, 'member'),
(8, 'ssisse6', 'fC7.~C4=?m4', 'Sherline', 'Sisse', '', '0', '', '', '', '', 0, 'member'),
(9, 'lplester7', 'vT5\'N+/Ts/K', 'Lynn', 'Plester', '', '0', '', '', '', '', 0, 'member'),
(10, 'tdevonside8', 'qW8+@YKn~s@&j', 'Teddie', 'Devonside', '', '0', '', '', '', '', 0, 'member'),
(11, 'gmccrachen9', 'wK1\"zP#y<#=03', 'Godfry', 'McCrachen', '', '0', '', '', '', '', 0, 'member'),
(12, 'dqueripela', 'fM3?tX_i\'#rUi', 'Ddene', 'Queripel', '', '0', '', '', '', '', 0, 'member'),
(13, 'ukirkebyeb', 'vG4\"<T#m6ZW{Z9_', 'Uriel', 'Kirkebye', '', '0', '', '', '', '', 0, 'member'),
(14, 'dschulkenc', 'xU0*D=E>51701GG', 'Drud', 'Schulken', '', '0', '', '', '', '', 0, 'member'),
(15, 'dstychd', 'rK7//(}S~/R', 'Darill', 'Stych', '', '0', '', '', '', '', 0, 'member'),
(16, 'bwestreye', 'mU9$Hp{7GhhUVQ.)', 'Bastian', 'Westrey', '', '0', '', '', '', '', 0, 'member'),
(17, 'bhessentalerf', 'pA8/z`,Bf{p)', 'Billy', 'Hessentaler', '', '0', '', '', '', '', 0, 'member'),
(18, 'bdoerfferg', 'cK5*EU6d', 'Baron', 'Doerffer', '', '0', '', '', '', '', 0, 'member'),
(19, 'ebathoh', 'tU8#.DIKby%', 'Edgardo', 'Batho', '', '0', '', '', '', '', 0, 'member'),
(20, 'gslynei', 'nQ6.n/QwI', 'Garey', 'Slyne', '', '0', '', '', '', '', 0, 'member'),
(21, 'pscourfieldj', 'hM5&3}nc=', 'Poul', 'Scourfield', '', '0', '', '', '', '', 0, 'member'),
(22, 'dheenank', 'qD7{#\".h{H', 'Donnajean', 'Heenan', '', '0', '', '', '', '', 0, 'member'),
(23, 'gsantryl', 'vR6?)nU`@`vY7', 'Gayelord', 'Santry', '', '0', '', '', '', '', 0, 'member'),
(24, 'tturviem', 'rI1)@5g%p_Z*I{H', 'Tedra', 'Turvie', '', '0', '', '', '', '', 0, 'member'),
(25, 'nnurcomben', 'nW9@/C`g_', 'Nolana', 'Nurcombe', '', '0', '', '', '', '', 0, 'member'),
(26, 'cgutowskao', 'rV9!=>s~p', 'Craggy', 'Gutowska', '', '0', '', '', '', '', 0, 'member'),
(27, 'ltenbrugp', 'sL8%EBGU\"43', 'Link', 'Tenbrug', '', '0', '', '', '', '', 0, 'member'),
(28, 'tpuviaq', 'zW4@ln|vBqbl', 'Tootsie', 'Puvia', '', '0', '', '', '', '', 0, 'member'),
(29, 'shearlr', 'tM1){`we\'lQy', 'Syman', 'Hearl', '', '0', '', '', '', '', 0, 'member'),
(30, 'dwinterburns', 'qZ9!3TG%=', 'Douglass', 'Winterburn', '', '0', '', '', '', '', 0, 'member'),
(31, 'khatchettt', 'fI9\'ua(,K*V', 'Kevyn', 'Hatchett', '', '0', '', '', '', '', 0, 'member'),
(32, 'drobbelu', 'yT4=&`&KAj', 'Derward', 'Robbel', '', '0', '', '', '', '', 0, 'member'),
(33, 'dbearmanv', 'wJ0=SDNU}ik${jP8', 'Dara', 'Bearman', '', '0', '', '', '', '', 0, 'member'),
(34, 'agrahlmansw', 'gV2&g>b~CLSAD&', 'Alia', 'Grahlmans', '', '0', '', '', '', '', 0, 'member'),
(35, 'closemannx', 'hN2,hAwaHh8Nrz', 'Clovis', 'Losemann', '', '0', '', '', '', '', 0, 'member'),
(36, 'sderingy', 'dD4&sI)W%5utJ', 'Sharlene', 'Dering', '', '0', '', '', '', '', 0, 'member'),
(37, 'etimmensz', 'nO9%<9M/wp4jPlrD', 'Emilio', 'Timmens', '', '0', '', '', '', '', 0, 'member'),
(38, 'dhaylock10', 'bG9.L1P{W@1o,', 'Dynah', 'Haylock', '', '0', '', '', '', '', 0, 'member'),
(39, 'ksmalcombe11', 'nH3%7=sxs~/LL', 'Kaitlin', 'Smalcombe', '', '0', '', '', '', '', 0, 'member'),
(40, 'pgiacomini12', 'yK7_Qu0eo', 'Porter', 'Giacomini', '', '0', '', '', '', '', 0, 'member'),
(41, 'hboughtwood13', 'eH8)<J6%Q?pYf}', 'Hew', 'Boughtwood', '', '0', '', '', '', '', 0, 'member'),
(42, 'glaurenzi14', 'vG4&,FhW!', 'Guilbert', 'Laurenzi', '', '0', '', '', '', '', 0, 'member'),
(43, 'cnaughton15', 'xA7}uo4v(LpdGW', 'Ches', 'Naughton', '', '0', '', '', '', '', 0, 'member'),
(44, 'minsley16', 'eT4/vvaOF1L', 'Minetta', 'Insley', '', '0', '', '', '', '', 0, 'member'),
(45, 'jflaxman17', 'yR7,|r5b!.\'w5(X', 'Jan', 'Flaxman', '', '0', '', '', '', '', 0, 'member'),
(46, 'lleavold18', 'dS6`A9vpE=KK`rQl', 'Leo', 'Leavold', '', '0', '', '', '', '', 0, 'member'),
(47, 'mredler19', 'uJ2_D\"O$!mpMHB', 'Miguel', 'Redler', '', '0', '', '', '', '', 0, 'member'),
(48, 'jdesport1a', 'vQ7}\"0)=D', 'Jenni', 'Desport', '', '0', '', '', '', '', 0, 'member'),
(49, 'mmarczyk1b', 'nB0<7s?@EIFD', 'Miranda', 'Marczyk', '', '0', '', '', '', '', 0, 'member'),
(50, 'ssmallwood1c', 'rL4<Jlo=', 'Stormi', 'Smallwood', '', '0', '', '', '', '', 0, 'member'),
(51, 'gspalding1d', 'vY4RN.#oS', 'Gerladina', 'Spalding', '', '0', '', '', '', '', 0, 'member'),
(52, 'fgiacobini1e', 'rE0.qqSn_\"7<=O', 'Fulton', 'Giacobini', '', '0', '', '', '', '', 0, 'member'),
(53, 'bstopp1', 'pW3!PaY#', 'Bernardina', 'Stopp', '', '0', '', '', '', '', 0, 'member'),
(54, 'frigeby2', 'oV0.kFWP%R!', 'Fletcher', 'Rigeby', '', '0', '', '', '', '', 0, 'member'),
(55, 'kcully3', 'hA4!|5$F4A_y2n', 'Karole', 'Cully', '', '0', '', '', '', '', 0, 'member'),
(56, 'cmelato4', 'gR2$vARWnK', 'Carline', 'Melato', '', '0', '', '', '', '', 0, 'member'),
(57, 'paberchirder5', 'kM0!11,cM3o', 'Phebe', 'Aberchirder', '', '0', '', '', '', '', 0, 'member'),
(58, 'ssisse6', 'fC7.~C4=?m4', 'Sherline', 'Sisse', '', '0', '', '', '', '', 0, 'member'),
(59, 'lplester7', 'vT5\'N+/Ts/K', 'Lynn', 'Plester', '', '0', '', '', '', '', 0, 'member'),
(60, 'tdevonside8', 'qW8+@YKn~s@&j', 'Teddie', 'Devonside', '', '0', '', '', '', '', 0, 'member'),
(61, 'gmccrachen9', 'wK1\"zP#y<#=03', 'Godfry', 'McCrachen', '', '0', '', '', '', '', 0, 'member'),
(62, 'dqueripela', 'fM3?tX_i\'#rUi', 'Ddene', 'Queripel', '', '0', '', '', '', '', 0, 'member'),
(63, 'ukirkebyeb', 'vG4\"<T#m6ZW{Z9_', 'Uriel', 'Kirkebye', '', '0', '', '', '', '', 0, 'member'),
(64, 'dschulkenc', 'xU0*D=E>51701GG', 'Drud', 'Schulken', '', '0', '', '', '', '', 0, 'member'),
(65, 'dstychd', 'rK7//(}S~/R', 'Darill', 'Stych', '', '0', '', '', '', '', 0, 'member'),
(66, 'bwestreye', 'mU9$Hp{7GhhUVQ.)', 'Bastian', 'Westrey', '', '0', '', '', '', '', 0, 'member'),
(67, 'bhessentalerf', 'pA8/z`,Bf{p)', 'Billy', 'Hessentaler', '', '0', '', '', '', '', 0, 'member'),
(68, 'bdoerfferg', 'cK5*EU6d', 'Baron', 'Doerffer', '', '0', '', '', '', '', 0, 'member'),
(69, 'ebathoh', 'tU8#.DIKby%', 'Edgardo', 'Batho', '', '0', '', '', '', '', 0, 'member'),
(70, 'gslynei', 'nQ6.n/QwI', 'Garey', 'Slyne', '', '0', '', '', '', '', 0, 'member'),
(71, 'pscourfieldj', 'hM5&3}nc=', 'Poul', 'Scourfield', '', '0', '', '', '', '', 0, 'member'),
(72, 'dheenank', 'qD7{#\".h{H', 'Donnajean', 'Heenan', '', '0', '', '', '', '', 0, 'member'),
(73, 'gsantryl', 'vR6?)nU`@`vY7', 'Gayelord', 'Santry', '', '0', '', '', '', '', 0, 'member'),
(74, 'tturviem', 'rI1)@5g%p_Z*I{H', 'Tedra', 'Turvie', '', '0', '', '', '', '', 0, 'member'),
(75, 'nnurcomben', 'nW9@/C`g_', 'Nolana', 'Nurcombe', '', '0', '', '', '', '', 0, 'member'),
(76, 'cgutowskao', 'rV9!=>s~p', 'Craggy', 'Gutowska', '', '0', '', '', '', '', 0, 'member'),
(77, 'ltenbrugp', 'sL8%EBGU\"43', 'Link', 'Tenbrug', '', '0', '', '', '', '', 0, 'member'),
(78, 'tpuviaq', 'zW4@ln|vBqbl', 'Tootsie', 'Puvia', '', '0', '', '', '', '', 0, 'member'),
(79, 'shearlr', 'tM1){`we\'lQy', 'Syman', 'Hearl', '', '0', '', '', '', '', 0, 'member'),
(80, 'dwinterburns', 'qZ9!3TG%=', 'Douglass', 'Winterburn', '', '0', '', '', '', '', 0, 'member'),
(81, 'khatchettt', 'fI9\'ua(,K*V', 'Kevyn', 'Hatchett', '', '0', '', '', '', '', 0, 'member'),
(82, 'drobbelu', 'yT4=&`&KAj', 'Derward', 'Robbel', '', '0', '', '', '', '', 0, 'member'),
(83, 'dbearmanv', 'wJ0=SDNU}ik${jP8', 'Dara', 'Bearman', '', '0', '', '', '', '', 0, 'member'),
(84, 'agrahlmansw', 'gV2&g>b~CLSAD&', 'Alia', 'Grahlmans', '', '0', '', '', '', '', 0, 'member'),
(85, 'closemannx', 'hN2,hAwaHh8Nrz', 'Clovis', 'Losemann', '', '0', '', '', '', '', 0, 'member'),
(86, 'sderingy', 'dD4&sI)W%5utJ', 'Sharlene', 'Dering', '', '0', '', '', '', '', 0, 'member'),
(87, 'etimmensz', 'nO9%<9M/wp4jPlrD', 'Emilio', 'Timmens', '', '0', '', '', '', '', 0, 'member'),
(88, 'dhaylock10', 'bG9.L1P{W@1o,', 'Dynah', 'Haylock', '', '0', '', '', '', '', 0, 'member'),
(89, 'ksmalcombe11', 'nH3%7=sxs~/LL', 'Kaitlin', 'Smalcombe', '', '0', '', '', '', '', 0, 'member'),
(90, 'pgiacomini12', 'yK7_Qu0eo', 'Porter', 'Giacomini', '', '0', '', '', '', '', 0, 'member'),
(91, 'hboughtwood13', 'eH8)<J6%Q?pYf}', 'Hew', 'Boughtwood', '', '0', '', '', '', '', 0, 'member'),
(92, 'glaurenzi14', 'vG4&,FhW!', 'Guilbert', 'Laurenzi', '', '0', '', '', '', '', 0, 'member'),
(93, 'cnaughton15', 'xA7}uo4v(LpdGW', 'Ches', 'Naughton', '', '0', '', '', '', '', 0, 'member'),
(94, 'minsley16', 'eT4/vvaOF1L', 'Minetta', 'Insley', '', '0', '', '', '', '', 0, 'member'),
(95, 'jflaxman17', 'yR7,|r5b!.\'w5(X', 'Jan', 'Flaxman', '', '0', '', '', '', '', 0, 'member'),
(96, 'lleavold18', 'dS6`A9vpE=KK`rQl', 'Leo', 'Leavold', '', '0', '', '', '', '', 0, 'member'),
(97, 'mredler19', 'uJ2_D\"O$!mpMHB', 'Miguel', 'Redler', '', '0', '', '', '', '', 0, 'member'),
(98, 'jdesport1a', 'vQ7}\"0)=D', 'Jenni', 'Desport', '', '0', '', '', '', '', 0, 'member'),
(99, 'mmarczyk1b', 'nB0<7s?@EIFD', 'Miranda', 'Marczyk', '', '0', '', '', '', '', 0, 'member'),
(100, 'ssmallwood1c', 'rL4<Jlo=', 'Stormi', 'Smallwood', '', '0', '', '', '', '', 0, 'member'),
(101, 'gspalding1d', 'vY4RN.#oS', 'Gerladina', 'Spalding', '', '0', '', '', '', '', 0, 'member'),
(102, 'fgiacobini1e', 'rE0.qqSn_\"7<=O', 'Fulton', 'Giacobini', '', '0', '', '', '', '', 0, 'member'),
(103, 'rrichardon0', 'yC6)w@Sg<', 'Roxana', 'Richardon', '', '0', '', '', '', '', 0, 'member'),
(104, 'bstopp1', 'pW3!PaY#', 'Bernardina', 'Stopp', '', '0', '', '', '', '', 0, 'member'),
(105, 'frigeby2', 'oV0.kFWP%R!', 'Fletcher', 'Rigeby', '', '0', '', '', '', '', 0, 'member'),
(106, 'kcully3', 'hA4!|5$F4A_y2n', 'Karole', 'Cully', '', '0', '', '', '', '', 0, 'member'),
(107, 'cmelato4', 'gR2$vARWnK', 'Carline', 'Melato', '', '0', '', '', '', '', 0, 'member'),
(108, 'paberchirder5', 'kM0!11,cM3o', 'Phebe', 'Aberchirder', '', '0', '', '', '', '', 0, 'member'),
(109, 'ssisse6', 'fC7.~C4=?m4', 'Sherline', 'Sisse', '', '0', '', '', '', '', 0, 'member'),
(110, 'lplester7', 'vT5\'N+/Ts/K', 'Lynn', 'Plester', '', '0', '', '', '', '', 0, 'member'),
(111, 'tdevonside8', 'qW8+@YKn~s@&j', 'Teddie', 'Devonside', '', '0', '', '', '', '', 0, 'member'),
(112, 'gmccrachen9', 'wK1\"zP#y<#=03', 'Godfry', 'McCrachen', '', '0', '', '', '', '', 0, 'member'),
(113, 'dqueripela', 'fM3?tX_i\'#rUi', 'Ddene', 'Queripel', '', '0', '', '', '', '', 0, 'member'),
(114, 'ukirkebyeb', 'vG4\"<T#m6ZW{Z9_', 'Uriel', 'Kirkebye', '', '0', '', '', '', '', 0, 'member'),
(115, 'dschulkenc', 'xU0*D=E>51701GG', 'Drud', 'Schulken', '', '0', '', '', '', '', 0, 'member'),
(116, 'dstychd', 'rK7//(}S~/R', 'Darill', 'Stych', '', '0', '', '', '', '', 0, 'member'),
(117, 'bwestreye', 'mU9$Hp{7GhhUVQ.)', 'Bastian', 'Westrey', '', '0', '', '', '', '', 0, 'member'),
(118, 'bhessentalerf', 'pA8/z`,Bf{p)', 'Billy', 'Hessentaler', '', '0', '', '', '', '', 0, 'member'),
(119, 'bdoerfferg', 'cK5*EU6d', 'Baron', 'Doerffer', '', '0', '', '', '', '', 0, 'member'),
(120, 'ebathoh', 'tU8#.DIKby%', 'Edgardo', 'Batho', '', '0', '', '', '', '', 0, 'member'),
(121, 'gslynei', 'nQ6.n/QwI', 'Garey', 'Slyne', '', '0', '', '', '', '', 0, 'member'),
(122, 'pscourfieldj', 'hM5&3}nc=', 'Poul', 'Scourfield', '', '0', '', '', '', '', 0, 'member'),
(123, 'dheenank', 'qD7{#\".h{H', 'Donnajean', 'Heenan', '', '0', '', '', '', '', 0, 'member'),
(124, 'gsantryl', 'vR6?)nU`@`vY7', 'Gayelord', 'Santry', '', '0', '', '', '', '', 0, 'member'),
(125, 'tturviem', 'rI1)@5g%p_Z*I{H', 'Tedra', 'Turvie', '', '0', '', '', '', '', 0, 'member'),
(126, 'nnurcomben', 'nW9@/C`g_', 'Nolana', 'Nurcombe', '', '0', '', '', '', '', 0, 'member'),
(127, 'cgutowskao', 'rV9!=>s~p', 'Craggy', 'Gutowska', '', '0', '', '', '', '', 0, 'member'),
(128, 'ltenbrugp', 'sL8%EBGU\"43', 'Link', 'Tenbrug', '', '0', '', '', '', '', 0, 'member'),
(129, 'tpuviaq', 'zW4@ln|vBqbl', 'Tootsie', 'Puvia', '', '0', '', '', '', '', 0, 'member'),
(130, 'shearlr', 'tM1){`we\'lQy', 'Syman', 'Hearl', '', '0', '', '', '', '', 0, 'member'),
(131, 'dwinterburns', 'qZ9!3TG%=', 'Douglass', 'Winterburn', '', '0', '', '', '', '', 0, 'member'),
(132, 'khatchettt', 'fI9\'ua(,K*V', 'Kevyn', 'Hatchett', '', '0', '', '', '', '', 0, 'member'),
(133, 'drobbelu', 'yT4=&`&KAj', 'Derward', 'Robbel', '', '0', '', '', '', '', 0, 'member'),
(134, 'dbearmanv', 'wJ0=SDNU}ik${jP8', 'Dara', 'Bearman', '', '0', '', '', '', '', 0, 'member'),
(135, 'agrahlmansw', 'gV2&g>b~CLSAD&', 'Alia', 'Grahlmans', '', '0', '', '', '', '', 0, 'member'),
(136, 'closemannx', 'hN2,hAwaHh8Nrz', 'Clovis', 'Losemann', '', '0', '', '', '', '', 0, 'member'),
(137, 'sderingy', 'dD4&sI)W%5utJ', 'Sharlene', 'Dering', '', '0', '', '', '', '', 0, 'member'),
(138, 'etimmensz', 'nO9%<9M/wp4jPlrD', 'Emilio', 'Timmens', '', '0', '', '', '', '', 0, 'member'),
(139, 'dhaylock10', 'bG9.L1P{W@1o,', 'Dynah', 'Haylock', '', '0', '', '', '', '', 0, 'member'),
(140, 'ksmalcombe11', 'nH3%7=sxs~/LL', 'Kaitlin', 'Smalcombe', '', '0', '', '', '', '', 0, 'member'),
(141, 'pgiacomini12', 'yK7_Qu0eo', 'Porter', 'Giacomini', '', '0', '', '', '', '', 0, 'member'),
(142, 'hboughtwood13', 'eH8)<J6%Q?pYf}', 'Hew', 'Boughtwood', '', '0', '', '', '', '', 0, 'member'),
(143, 'glaurenzi14', 'vG4&,FhW!', 'Guilbert', 'Laurenzi', '', '0', '', '', '', '', 0, 'member'),
(144, 'cnaughton15', 'xA7}uo4v(LpdGW', 'Ches', 'Naughton', '', '0', '', '', '', '', 0, 'member'),
(145, 'minsley16', 'eT4/vvaOF1L', 'Minetta', 'Insley', '', '0', '', '', '', '', 0, 'member'),
(146, 'jflaxman17', 'yR7,|r5b!.\'w5(X', 'Jan', 'Flaxman', '', '0', '', '', '', '', 0, 'member'),
(147, 'lleavold18', 'dS6`A9vpE=KK`rQl', 'Leo', 'Leavold', '', '0', '', '', '', '', 0, 'member'),
(148, 'mredler19', 'uJ2_D\"O$!mpMHB', 'Miguel', 'Redler', '', '0', '', '', '', '', 0, 'member'),
(149, 'jdesport1a', 'vQ7}\"0)=D', 'Jenni', 'Desport', '', '0', '', '', '', '', 0, 'member'),
(150, 'mmarczyk1b', 'nB0<7s?@EIFD', 'Miranda', 'Marczyk', '', '0', '', '', '', '', 0, 'member'),
(151, 'ssmallwood1c', 'rL4<Jlo=', 'Stormi', 'Smallwood', '', '0', '', '', '', '', 0, 'member'),
(152, 'gspalding1d', 'vY4RN.#oS', 'Gerladina', 'Spalding', '', '0', '', '', '', '', 0, 'member'),
(153, 'fgiacobini1e', 'rE0.qqSn_\"7<=O', 'Fulton', 'Giacobini', '', '0', '', '', '', '', 0, 'member'),
(154, 'kstredwick1f', 'xV2#Qj|ndI', 'Kora', 'Stredwick', '', '0', '', '', '', '', 0, 'member'),
(155, 'ibroy1g', 'vA7!epRfq)r\'g', 'Irwin', 'Broy', '', '0', '', '', '', '', 0, 'member'),
(156, 'jcarne1h', 'bY3}}8kk0QU', 'Jeramey', 'Carne', '', '0', '', '', '', '', 0, 'member'),
(157, 'tmacaless1i', 'kE2)82d3#7ma@', 'Tarrance', 'MacAless', '', '0', '', '', '', '', 0, 'member'),
(158, 'aiaduccelli1j', 'fC2#$|/KLqYU_N', 'Anthiathia', 'Iaduccelli', '', '0', '', '', '', '', 0, 'member'),
(159, 'jsinkins1k', 'yY5\"#7?\'', 'Juanita', 'Sinkins', '', '0', '', '', '', '', 0, 'member'),
(160, 'jchastanet1l', 'gY8)6*<aUd2Y,)', 'Joletta', 'Chastanet', '', '0', '', '', '', '', 0, 'member'),
(161, 'jgribben1m', 'iJ4=`2<Q3Icr*7', 'Joni', 'Gribben', '', '0', '', '', '', '', 0, 'member'),
(162, 'oscreeton1n', 'bY4}Bje`=Je>', 'Oralia', 'Screeton', '', '0', '', '', '', '', 0, 'member'),
(163, 'wspada1o', 'sX8,k0mw#u<bt', 'Welsh', 'Spada', '', '0', '', '', '', '', 0, 'member'),
(164, 'dkuschek1p', 'wJ6ULgZr}7K}}', 'Delila', 'Kuschek', '', '0', '', '', '', '', 0, 'member'),
(165, 'ohymor1q', 'gY5\'1h>p', 'Onofredo', 'Hymor', '', '0', '', '', '', '', 0, 'member'),
(166, 'mmalloch1r', 'eP5`&q2<bsr?LnF', 'Miriam', 'Malloch', '', '0', '', '', '', '', 0, 'member'),
(167, 'scakebread1s', 'mG6*6j$dlT73V$jt', 'Stacey', 'Cakebread', '', '0', '', '', '', '', 0, 'member'),
(168, 'hfarman1t', 'mP7/\'`H~%py1rC', 'Hugo', 'Farman', '', '0', '', '', '', '', 0, 'member'),
(169, 'jkindall1u', 'tM0./_BuKP_', 'Jaymie', 'Kindall', '', '0', '', '', '', '', 0, 'member'),
(170, 'shellard1v', 'cX7}jddq%', 'Suzanne', 'Hellard', '', '0', '', '', '', '', 0, 'member'),
(171, 'kflacknoe22', 'zG3`6NVDz|', 'Kristyn', 'Flacknoe', '', '0', '', '', '', '', 0, 'member'),
(172, 'klempertz0', '6d658c828e502b671f0d16bb5ef07be4', 'Katinka', 'Lempertz', '', '0', '', '', '', '', 0, 'member'),
(173, 'ctillyer1', 'bccb53e5ab036719b56dd205a4fef5d6', 'Chevy', 'Tillyer', '', '0', '', '', '', '', 0, 'member'),
(174, 'lstanyforth2', '0daf78f8859e563275200b3b8146c881', 'Lou', 'Stanyforth', '', '0', '', '', '', '', 0, 'member'),
(175, 'ksphinxe3', 'ce0f8f25e03b00787f7821cfcf645f2e', 'Karie', 'Sphinxe', '', '0', '', '', '', '', 0, 'member'),
(176, 'fblasetti4', 'dd4afd32232856829a532dfe783a2145', 'Fanny', 'Blasetti', '', '0', '', '', '', '', 0, 'member'),
(177, 'hwhorlow5', 'c8bad00a1dce3904d1dce0b9f703c4d1', 'Harlan', 'Whorlow', '', '0', '', '', '', '', 0, 'member'),
(178, 'amatelyunas6', '850d630aa5b690e6cb5853b947507667', 'Avery', 'Matelyunas', '', '0', '', '', '', '', 0, 'member'),
(179, 'mmaccague7', '17bff351e0cc504aa657a814f14dfec8', 'Maria', 'MacCague', '', '0', '', '', '', '', 0, 'member'),
(180, 'ashervil8', '78b4cc379291e4c890e53127d9840aeb', 'Anestassia', 'Shervil', '', '0', '', '', '', '', 0, 'member'),
(181, 'dgower9', '24f5765b116298fceb70f82322756ae3', 'Di', 'Gower', '', '0', '', '', '', '', 0, 'member'),
(182, 'mhindricha', '3073cd6853c94d3a207f06535ebdf47d', 'Margy', 'Hindrich', '', '0', '', '', '', '', 0, 'member'),
(183, 'hriggulsfordb', '28c6120e85551f26c5a9a631eddb4275', 'Hanny', 'Riggulsford', '', '0', '', '', '', '', 0, 'member'),
(184, 'jgimbrettc', '81d4be54c756811444497733747f06e3', 'Jennica', 'Gimbrett', '', '0', '', '', '', '', 0, 'member'),
(185, 'wlindld', '6ab3c67e8b73a44b4c04b8f18356b5f3', 'Wrennie', 'Lindl', '', '0', '', '', '', '', 0, 'member'),
(186, 'telnore', '9cf6592fda9adc4f89cff558a406413e', 'Travus', 'Elnor', '', '0', '', '', '', '', 0, 'member'),
(187, 'amakinsonf', '60157301acb33e532728d25ce70a733c', 'Augustine', 'Makinson', '', '0', '', '', '', '', 0, 'member'),
(188, 'zbalchg', 'f7ecdead9349597923133fddf9d1bd83', 'Zonda', 'Balch', '', '0', '', '', '', '', 0, 'member'),
(189, 'dandriuzzih', 'd99ed152da3466a6b2dfdf2905ba89be', 'Dolph', 'Andriuzzi', '', '0', '', '', '', '', 0, 'member'),
(190, 'ecrowderi', '9de9e38166e3a94a54e4e24f9a9d6a17', 'Evin', 'Crowder', '', '0', '', '', '', '', 0, 'member'),
(191, 'wtillj', 'a85307779c43980b1330068fdab136c9', 'Winn', 'Till', '', '0', '', '', '', '', 0, 'member'),
(192, 'cmurchk', 'c2100a9deb3e8a4974c6488f8d6e9335', 'Cathie', 'Murch', '', '0', '', '', '', '', 0, 'member'),
(193, 'cleylandl', '759f6ef5898afbfdec8c3db79eb15438', 'Coreen', 'Leyland', '', '0', '', '', '', '', 0, 'member'),
(194, 'olelievrem', '8cfdd6c0048e05f840add28f047dde86', 'Opal', 'Le Lievre', '', '0', '', '', '', '', 0, 'member'),
(195, 'bosgordbyn', '8183206977f25c309a26d415e71b31b7', 'Buffy', 'Osgordby', '', '0', '', '', '', '', 0, 'member'),
(196, 'jcurdo', '52b1992037b81dfde9b922853c536ddd', 'Jacquie', 'Curd', '', '0', '', '', '', '', 0, 'member'),
(197, 'adwyrp', '1e0dbf301c916cd2119a906389a06e6a', 'Arther', 'Dwyr', '', '0', '', '', '', '', 0, 'member'),
(198, 'plidgertonq', '176016a02870889e602805531a554a39', 'Peadar', 'Lidgerton', '', '0', '', '', '', '', 0, 'member'),
(199, 'rluckinr', '8bea1125065522dcdac5bd2f65b35703', 'Ramon', 'Luckin', '', '0', '', '', '', '', 0, 'member'),
(200, 'dcawleys', '3984cdf7d701a86fe3bff9066c50cfcb', 'Demetrius', 'Cawley', '', '0', '', '', '', '', 0, 'member'),
(201, 'yemanuelovt', '12b1b03f7451729683f6e18393ceb932', 'Yoshiko', 'Emanuelov', '', '0', '', '', '', '', 0, 'member'),
(202, 'epreeceu', 'e8a84f55c159a87070f07acd4028142f', 'Esmaria', 'Preece', '', '0', '', '', '', '', 0, 'member'),
(203, 'ecopov', '4277c008c329130402d0b8ab419be5a2', 'Emmit', 'Copo', '', '0', '', '', '', '', 0, 'member'),
(204, 'kblomfieldw', '6fa657932b870e2ea6f848fbe314914f', 'Kassey', 'Blomfield', '', '0', '', '', '', '', 0, 'member'),
(205, 'auphamx', '505814e1d7acf18aa640163ab85a8ef3', 'Arnold', 'Upham', '', '0', '', '', '', '', 0, 'member'),
(206, 'smoorerudy', '9000e56a0cf0aa27ed880a750765354f', 'Syd', 'Moorerud', '', '0', '', '', '', '', 0, 'member'),
(207, 'mroperz', '63ab80d644c8603104ea72c43be4a437', 'Mickie', 'Roper', '', '0', '', '', '', '', 0, 'member'),
(208, 'hoheffernan10', '06a60206a20313cbe45d4f04ea790534', 'Had', 'O\'Heffernan', '', '0', '', '', '', '', 0, 'member'),
(209, 'mshapira11', 'e4a9c50b51c5c640d078e6e07fb0cea6', 'Melessa', 'Shapira', '', '0', '', '', '', '', 0, 'member'),
(210, 'ktigner12', '7669c07103257a1d4d745e85ebafba1e', 'Kathye', 'Tigner', '', '0', '', '', '', '', 0, 'member'),
(211, 'hagney13', '9c4ceda1daa3259f5eb932b3a6da89ba', 'Harv', 'Agney', '', '0', '', '', '', '', 0, 'member'),
(212, 'gachurch14', '1b4bcab2c4adce2b3ffc289df2169da6', 'Gertrude', 'Achurch', '', '0', '', '', '', '', 0, 'member'),
(213, 'jdrayton15', 'e5e37d30a642094728ec9f6af1a82e08', 'Jeniece', 'Drayton', '', '0', '', '', '', '', 0, 'member'),
(214, 'cjayne16', 'f8b33253c32ec87bad3fe88ee7f47fe8', 'Caren', 'Jayne', '', '0', '', '', '', '', 0, 'member'),
(215, 'gmedlin17', 'd070a3f2259fac679ecfc4cb29f0a52e', 'Garreth', 'Medlin', '', '0', '', '', '', '', 0, 'member'),
(216, 'mswendell18', '0363267d9a24ce338bc54d5316208d79', 'Montague', 'Swendell', '', '0', '', '', '', '', 0, 'member'),
(217, 'pbehneke19', 'c03bbeb3cd844dc5a89392b0bb7400aa', 'Perren', 'Behneke', '', '0', '', '', '', '', 0, 'member'),
(218, 'rlegrand1a', 'c36c12dddaaa40ab4ef5739392a83ef5', 'Rora', 'Legrand', '', '0', '', '', '', '', 0, 'member'),
(219, 'vbottby1b', '1e5ba4a5b7611d6bafdb2d1178408b08', 'Vanny', 'Bottby', '', '0', '', '', '', '', 0, 'member'),
(220, 'gabrahams1c', 'e2478c847818867b3cd8f0701b1ec855', 'Giordano', 'Abrahams', '', '0', '', '', '', '', 0, 'member'),
(221, 'cmcdiarmid1d', '1141ad1dd5ea277129213168f70decbb', 'Charles', 'McDiarmid', '', '0', '', '', '', '', 0, 'member'),
(222, 'wcool1e', 'b42b75eb692ddfbfa4d71d50533ef7db', 'Walton', 'Cool', '', '0', '', '', '', '', 0, 'member'),
(223, 'kcoomes1f', '3cbf9adb8507da5f6dee368545b24bc1', 'Kristopher', 'Coomes', '', '0', '', '', '', '', 0, 'member'),
(224, 'jdallison1g', 'c63bc76e11d641aaf01049de59c872cd', 'Jori', 'Dallison', '', '0', '', '', '', '', 0, 'member'),
(225, 'kstede1h', '30c1e347eb8e43c2e179d85c04a8b11e', 'Kippy', 'Stede', '', '0', '', '', '', '', 0, 'member'),
(226, 'vhawken1i', 'b6460f9faabe18827df0f26a26ad7241', 'Vina', 'Hawken', '', '0', '', '', '', '', 0, 'member'),
(227, 'rcrank1j', '37578b4290509e62d4b8b30bf2f3df6a', 'Raphael', 'Crank', '', '0', '', '', '', '', 0, 'member'),
(228, 'btosney1k', 'f43e1e876337b4d2d0a91cd0ecedd258', 'Biddy', 'Tosney', '', '0', '', '', '', '', 0, 'member'),
(229, 'dbalderston1l', 'bf0af993ed1177a8afce026e6d69afb7', 'Dyanne', 'Balderston', '', '0', '', '', '', '', 0, 'member'),
(230, 'jangrove1m', '691b4119a48341aa7ab4bf746fb39f74', 'Janey', 'Angrove', '', '0', '', '', '', '', 0, 'member'),
(231, 'cmundall1n', '092902417f77b9e62c52c89b97c7c5f9', 'Carmelle', 'Mundall', '', '0', '', '', '', '', 0, 'member'),
(232, 'erettie1o', '39d1690f55fe09610d0f5ad9739566fc', 'Emmery', 'Rettie', '', '0', '', '', '', '', 0, 'member'),
(233, 'gmoorman1p', 'db73e09d5b0cfac50f023b514f29941a', 'Gradeigh', 'Moorman', '', '0', '', '', '', '', 0, 'member'),
(234, 'ntivnan1q', '1f8b2fc9d2ec0eeeacbfa21b46e09476', 'Nessi', 'Tivnan', '', '0', '', '', '', '', 0, 'member'),
(235, 'gpaynter1r', '0daa2af2f7d01c68dae825afb45ab2cd', 'Gun', 'Paynter', '', '0', '', '', '', '', 0, 'member'),
(236, 'apostle1s', 'd3744c497610248c1ebb8db0e0782d61', 'Austin', 'Postle', '', '0', '', '', '', '', 0, 'member'),
(237, 'dfranken1t', '872ae11633c45cce04ed0d3e22f3d8fa', 'Darice', 'Franken', '', '0', '', '', '', '', 0, 'member'),
(238, 'wgoosnell1u', 'b3fd3da1edf64a6cfd2cedd20589fb14', 'Whitby', 'Goosnell', '', '0', '', '', '', '', 0, 'member'),
(239, 'whamp1v', '2c34abb411ac0b1ce40571d47193619b', 'Wilmette', 'Hamp', '', '0', '', '', '', '', 0, 'member'),
(240, 'hvennings1w', 'c665c1ff929217529d9230af6809a01b', 'Harlie', 'Vennings', '', '0', '', '', '', '', 0, 'member'),
(241, 'mruddoch1x', '86c32dbf6cb1a3c042df4c0882748f11', 'Maddy', 'Ruddoch', '', '0', '', '', '', '', 0, 'member'),
(242, 'dbalam1y', 'e70e8b29ce694fa8806e3d9a081b1502', 'Dael', 'Balam', '', '0', '', '', '', '', 0, 'member'),
(243, 'gmarch1z', 'bbeb07d18e0400ae4764c7eb92019b1b', 'Garnet', 'March', '', '0', '', '', '', '', 0, 'member'),
(244, 'ctregear20', '3ecba41b3d1c5b505c1afd5d8ecc2bf3', 'Caressa', 'Tregear', '', '0', '', '', '', '', 0, 'member'),
(245, 'rlinay21', '4f7cef34efd919476226ea923a9deed8', 'Rhodie', 'Linay', '', '0', '', '', '', '', 0, 'member'),
(246, 'rhakes22', '9eacda78969ad4c4d117734998033957', 'Retha', 'Hakes', '', '0', '', '', '', '', 0, 'member'),
(247, 'bodam23', '7a0e6b9b12832a6e6d5f221e02935af7', 'Boone', 'Odam', '', '0', '', '', '', '', 0, 'member'),
(248, 'gcartner24', '1969929b703094b63964d691ec677738', 'Gwenny', 'Cartner', '', '0', '', '', '', '', 0, 'member'),
(249, 'ihancorn25', '3990665de46af5fde952fefe8c78d16d', 'Idelle', 'Hancorn', '', '0', '', '', '', '', 0, 'member'),
(250, 'kcossom26', '9181d39a9938ccae64f61606da420849', 'Kermit', 'Cossom', '', '0', '', '', '', '', 0, 'member'),
(251, 'nsalliere27', 'afa14a5627c1fee537908a1737ac61b5', 'Nester', 'Salliere', '', '0', '', '', '', '', 0, 'member'),
(252, 'parmit28', 'ebd2c171bf6451728316cc4fd31d1377', 'Putnam', 'Armit', '', '0', '', '', '', '', 0, 'member'),
(253, 'kdacres29', 'd87b0562573773e3977584a3108d169d', 'Katusha', 'Dacres', '', '0', '', '', '', '', 0, 'member'),
(254, 'dforster2a', 'e35dc8f235f58d5a29923f80430e8659', 'Dore', 'Forster', '', '0', '', '', '', '', 0, 'member'),
(255, 'mkesley2b', 'e8719f143c1095b2205dbb7584fbb487', 'Mella', 'Kesley', '', '0', '', '', '', '', 0, 'member'),
(256, 'doyley2c', '59ff2412a397028181e21b8f34af31df', 'Deanna', 'Oyley', '', '0', '', '', '', '', 0, 'member'),
(257, 'cgregory2d', '895c79cd4500dbac53b2071bb844ba3e', 'Calley', 'Gregory', '', '0', '', '', '', '', 0, 'member'),
(258, 'jronchetti2e', '3fe4a57baae77c3889a1f56f965d02fc', 'Jamey', 'Ronchetti', '', '0', '', '', '', '', 0, 'member'),
(259, 'wodonegan2f', '9fee79772f66cb815fea0b46864a83d6', 'Wiatt', 'O\'Donegan', '', '0', '', '', '', '', 0, 'member'),
(260, 'mmaskelyne2g', '055c5d1db9a7a3c84ac7726f2ffc895e', 'Marya', 'Maskelyne', '', '0', '', '', '', '', 0, 'member'),
(261, 'rsoden2h', '57f77572692b08b9713dafff226463b4', 'Ruddie', 'Soden', '', '0', '', '', '', '', 0, 'member'),
(262, 'gkohnemann2i', '39e6d7d29cfb0748302ac2b46467027f', 'Gavin', 'Kohnemann', '', '0', '', '', '', '', 0, 'member'),
(263, 'jdunton2j', '2515967955bb1d5ecc2093026c7a95c7', 'Jordanna', 'Dunton', '', '0', '', '', '', '', 0, 'member'),
(264, 'mmeddemmen2k', '7c6e98173c1afdd2352798212ed44abb', 'Merell', 'Meddemmen', '', '0', '', '', '', '', 0, 'member'),
(265, 'cperet2l', '5c819e7941790dbf18bf96c17da3f050', 'Cornell', 'Peret', '', '0', '', '', '', '', 0, 'member'),
(266, 'kerb2m', '28e3c6046496ae51b63bfc0cdd065289', 'Kippar', 'Erb', '', '0', '', '', '', '', 0, 'member'),
(267, 'odiss2n', '0b2f0a3e11107ed1776c0c6d56f75208', 'Orran', 'Diss', '', '0', '', '', '', '', 0, 'member'),
(268, 'jmcfadden2o', '025544c49355b9ae7e5fc8799b33dda8', 'Janeen', 'McFadden', '', '0', '', '', '', '', 0, 'member'),
(269, 'gbefroy2p', '597ecebf067ba3c510d49c828b9af450', 'Guntar', 'Befroy', '', '0', '', '', '', '', 0, 'member'),
(270, 'laves2q', '32561a968ad8b3adb9ae43e06088e80b', 'Lombard', 'Aves', '', '0', '', '', '', '', 0, 'member'),
(271, 'rblundin2r', '91d2e127863f0a7cd86942bf15002fb0', 'Raymund', 'Blundin', '', '0', '', '', '', '', 0, 'member'),
(337, 'Sing003', '4a094858b33da6d33ff6bdb99ea4034c', 'Sukasa', 'Saeheng', 'Sing', '0808867088', '', '', '', '', 0, 'member'),
(338, 'Sing004', '4a094858b33da6d33ff6bdb99ea4034c', 'Sukasa', 'Saeheng', 'Sing', '0808867088', '354/68 หมู่ 2', 'บางบัวทอง', 'บางบัวทอง', 'นนทบุรี', 11110, 'member'),
(343, 'Sing002', '4a094858b33da6d33ff6bdb99ea4034c', 'Sukasa', 'Saeheng', 'sing', '', '', '', '', '', 0, 'member'),
(345, 'sksmaster1', '4a094858b33da6d33ff6bdb99ea4034c', 'Sukasa', 'Saehemg', 'Sing', '', '', '', '', '', 0, 'member');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idproduct` int(11) NOT NULL,
  `nameproduct` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `brand` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `img_profile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`idproduct`, `nameproduct`, `description`, `brand`, `price`, `quantity`, `img_profile`) VALUES
(1, 'Jordan Sport', 'เสื้อแข่งตาข่ายผู้ชาย Dri-FIT', 2, 1600, 500, 'https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco,u_126ab356-44d8-4a06-89b4-fcdcc8df0245,c_scale,fl_relative,w_1.0,h_1.0,fl_layer_apply/58e0183f-4d4d-42af-9d29-b90fcc1d4cc7/%E0%B9%80%E0%B8%AA%E0%B8%B7%E0%B9%89%E0%B8%AD%E0%B9%81%E0%B8%82%E0%B9%88%E0%B8%87%E0%B8%95%E0%B8%B2%E0%B8%82%E0%B9%88%E0%B8%B2%E0%B8%A2%E0%B8%9C%E0%B8%B9%E0%B9%89-dri-fit-jordan-sport-r4GjTV.png'),
(2, 'Jordan Flight MVP', 'เสื้อยืดผู้ชาย\r\n', 2, 1300, 500, 'https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco,u_126ab356-44d8-4a06-89b4-fcdcc8df0245,c_scale,fl_relative,w_1.0,h_1.0,fl_layer_apply/85c82585-6bd7-43e5-87fd-a50d4e08c864/%E0%B9%80%E0%B8%AA%E0%B8%B7%E0%B9%89%E0%B8%AD%E0%B8%A2%E0%B8%B7%E0%B8%94%E0%B8%9C%E0%B8%B9%E0%B9%89-jordan-flight-mvp-MqkK9P.png'),
(3, 'Nike ACG \"Goat Rocks\"', 'เสื้อแขนสั้นผู้ชาย Dri-FIT ADV UV', 2, 1700, 500, 'https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco/a3f959a1-c879-4e29-93ca-ffe34e36c620/%E0%B9%80%E0%B8%AA%E0%B8%B7%E0%B9%89%E0%B8%AD%E0%B9%81%E0%B8%82%E0%B8%99%E0%B8%AA%E0%B8%B1%E0%B9%89%E0%B8%99%E0%B8%9C%E0%B8%B9%E0%B9%89-dri-fit-adv-uv-acg-goat-rocks-DqFhXq.png'),
(4, 'Nike Sportswear', 'เสื้อวอร์มคอกลมผ้าเฟรนช์เทรีผู้ชาย', 2, 2300, 500, 'https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco/d226f677-14ab-41e8-be02-f286f22c3fb3/%E0%B9%80%E0%B8%AA%E0%B8%B7%E0%B9%89%E0%B8%AD%E0%B8%A7%E0%B8%AD%E0%B8%A3%E0%B9%8C%E0%B8%A1%E0%B8%84%E0%B8%AD%E0%B8%81%E0%B8%A5%E0%B8%A1%E0%B8%9C%E0%B9%89%E0%B8%B2%E0%B9%80%E0%B8%9F%E0%B8%A3%E0%B8%99%E0%B8%8A%E0%B9%8C%E0%B9%80%E0%B8%97%E0%B8%A3%E0%B8%B5%E0%B8%9C%E0%B8%B9%E0%B9%89-sportswear-GVCr8q.png'),
(5, 'Kevin Durant Select Series', 'เสื้อยืด Nike NBA ผู้ชาย', 2, 1400, 500, 'https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco/e6767202-1d11-4f3c-a659-8ae9b410c48e/%E0%B9%80%E0%B8%AA%E0%B8%B7%E0%B9%89%E0%B8%AD%E0%B8%A2%E0%B8%B7%E0%B8%94-nba-%E0%B8%9C%E0%B8%B9%E0%B9%89-kevin-durant-select-series-HSdP6B.png'),
(6, 'เสื้อยืด U คอกลม แขนสั้น', 'เสื้อยืดเรียบง่ายที่สมบูรณ์แบบเหมาะกับทุกสไตล์ เนื้อผ้าและรูปทรงสวยงาม ผ้าคอตตอน 100% ให้สัมผัสเนียนนุ่ม', 1, 390, 300, 'https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/424873/sub/goods_424873_sub14.jpg?width=750'),
(7, 'เสื้อยืดแขนสั้น MICKEY MOUSE IN THAILAND UT', 'พบกับ Mickey Mouse และผองเพื่อนในประเทศไทยได้แล้ววันนี้!', 1, 490, 300, 'https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/468499/item/goods_52_468499.jpg?width=750'),
(8, 'เสื้อยืดแขนสั้น MICKEY MOUSE IN THAILAND UT', 'พบกับ Mickey Mouse และผองเพื่อนในประเทศไทยได้แล้ววันนี้!', 1, 490, 300, 'https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/468498/item/goods_70_468498.jpg?width=750'),
(9, 'เสื้อยืดแขนสั้น MICKEY MOUSE IN THAILAND UT', 'พบกับ Mickey Mouse และผองเพื่อนในประเทศไทยได้แล้ววันนี้!', 1, 490, 300, 'พบกับ Mickey Mouse และผองเพื่อนในประเทศไทยได้แล้ววันนี้!'),
(10, 'เสื้อยืดแขนสั้น STUDIO GHIBLI UT', 'คอลเลคชันแบบออริจินอลที่เผยให้เห็นจักรวาลของสตูดิโอจิบลิ (Studio Ghibli) ในหลากหลายรูปแบบ', 1, 590, 300, 'https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/466795/item/goods_09_466795.jpg?width=750'),
(11, 'เสื้อยืดแขนสั้น UT ARCHIVE Jean-Michel Basquiat UT', 'ไอเทมเหนือกาลเวลา ไอเทมที่ผ่านการคัดสรรจากคอลเลคชันเสื้อยืด UT ที่เคยวางจำหน่ายในอดีตจะกลับมามีชีวิตชีวาอีกครั้ง\r\n', 1, 590, 500, 'https://image.uniqlo.com/UQ/ST3/AsianCommon/imagesgoods/463154/item/goods_00_463154.jpg?width=750'),
(12, 'Nike Air Force 1 Shadow', 'รวมทุกสิ่งที่คุณรักใน AF1 แต่เพิ่มเป็น 2 เท่า! Air Force 1 Shadow ใส่ความขี้เล่นให้กับดีไซน์บาสเก็ตบอลแบบคลาสสิก วัสดุหลายเลเยอร์ให้ลุคที่มีมิติ ขณะที่พื้นรองเท้าชั้นกลางขนาดใหญ่และแบรนดิ้งคู่ทำให้สนีกเกอร์คู่นี้ดูสดใหม่และมีสไตล์เป็น 2 เท่า', 2, 5200, 50, 'รองเท้าผู้-air-force-1-shadow-g2s2Kc.png'),
(13, 'Nike Air Force 1 Shadow', 'รวมทุกสิ่งที่คุณรักใน AF1 แต่เพิ่มเป็น 2 เท่า! Air Force 1 Shadow ใส่ความขี้เล่นให้กับดีไซน์บาสเก็ตบอลแบบคลาสสิก วัสดุหลายเลเยอร์ให้ลุคที่มีมิติ ขณะที่พื้นรองเท้าชั้นกลางขนาดใหญ่และแบรนดิ้งคู่ทำให้สนีกเกอร์คู่นี้ดูสดใหม่และมีสไตล์เป็น 2 เท่า', 2, 5200, 50, 'รองเท้าผู้-air-force-1-shadow-g2s2Kc.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idproduct`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=346;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `idproduct` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
