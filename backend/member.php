<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- head -->
  <?php require_once("template/head.php"); ?>

</head>

<body>
  <!-- navber -->
  <?php require_once("template/navber.php"); ?>
  <!-- container -->
  <div class="dashboard-content px-3 pt-4">

    <div class="container-fluid mt-5">
      <div class="card" style="width: 100%;">
        <div class="card-body">
          <button type="button" class="btn btn-primary createmember" data-bs-toggle="modal" data-bs-target="#creatememberModal">
            Create Member
          </button>
          <div class="table-responsive">
          <table id="myTable" class="display table" style="width: 100%;" >
            <thead>
              <tr>
                <th>ลำดับ</th>
                <th>id_member</th>
                <th>username</th>
                <th>name</th>
                <th>lname</th>
                <th>nickname</th>
                <th>tel</th>
                <th>ดูข้อมูล</th>
                <th>การจัดการ</th>
              </tr>
            </thead>
            <tbody id="showmember">

            </tbody>
          </table>

        </div>
      </div>
    </div>
</div>
    <!-- modal create member -->
    <div class="modal fade" id="creatememberModal" tabindex="-1" aria-labelledby="creatememberModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h1 class="modal-title fs-5" id="creatememberModalLabel">Create Member</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="username" class="form-label">Username</label>
                  <input type="text" class="form-control" id="username">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="Password" class="form-label">Password</label>
                  <input type="password" class="form-control" id="Password">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="name" class="form-label">Real name</label>
                  <input type="text" class="form-control" id="createname">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="lname" class="form-label">Last name</label>
                  <input type="text" class="form-control" id="createlname">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="Nickname" class="form-label">Nickname</label>
                  <input type="text" class="form-control" id="createNickname">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-primary Create">create memder</button>
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- modal show member -->
    <div class="modal fade" id="showmemberModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h1 class="modal-title fs-5" id="exampleModalLabel">data Member</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="name" class="form-label">Real name</label>
                  <input type="text" class="form-control" id="name" disabled>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="lname" class="form-label">Last name</label>
                  <input type="text" class="form-control" id="lname" disabled>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="Nickname" class="form-label">Nickname</label>
                  <input type="text" class="form-control" id="Nickname" disabled>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="tel" class="form-label">tel</label>
                  <input type="text" class="form-control" id="tel" disabled>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="address" class="form-label">address</label>
                  <input type="text" class="form-control" id="address" disabled>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="sub_district" class="form-label">sub_district</label>
                  <input type="text" class="form-control" id="sub_district" disabled>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="district" class="form-label">district</label>
                  <input type="text" class="form-control" id="district" disabled>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="province" class="form-label">province</label>
                  <input type="text" class="form-control" id="province" disabled>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="postcode" class="form-label">postcode</label>
                  <input type="text" class="form-control" id="postcode" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


    <!-- modal edit member -->
    <div class="modal fade" id="editmemberModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Member</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="name" class="form-label">Real name</label>
                  <input type="text" class="form-control" id="editname">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="lname" class="form-label">Last name</label>
                  <input type="text" class="form-control" id="editlname">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="Nickname" class="form-label">Nickname</label>
                  <input type="text" class="form-control" id="editNickname">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="tel" class="form-label">tel</label>
                  <input type="text" class="form-control" id="edittel">
                </div>
              </div>

              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="address" class="form-label">address</label>
                  <input type="text" class="form-control" id="editaddress">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="mb-3">
                  <label for="sub_district" class="form-label">sub_district</label>
                  <input type="text" class="form-control" id="editsub_district">
                </div>
              </div>

              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="district" class="form-label">district</label>
                  <input type="text" class="form-control" id="editdistrict">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="province" class="form-label">province</label>
                  <input type="text" class="form-control" id="editprovince">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="mb-3">
                  <label for="postcode" class="form-label">postcode</label>
                  <input type="text" class="form-control" id="editpostcode">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary edit">edit member</button>
          </div>
        </div>
      </div>
    </div>

    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="dist/js/connet_server.js"></script>

</body>

</html>
<!-- เป็นการสร้าง member แบบ API -->
 <script src="dist/js/member/createmember.js"></script>

<!-- การดึงข้อมูล API ทั้งหมด มา โช โดยทำการ add เข้า DataTable -->
 <script src="dist/js/member/show_readmember.js"></script>

<!-- เป็นการ ข้อมูลเพิ่มเติม ของ idmember นั้น โดยจะทำการ โช แบบ modal-->
 <script src="dist/js/member/showdatamemberone.js"></script>


<!-- เป็นการ editข้อมูล หรือ แก้ไขข้อมูล ของ idmember นั้น โดยจะทำการ โช แบบ modal -->
 <script src="dist/js/member/edit_update_member.js"></script>


<!-- การลบข้อมูล member -->
<script>
  function removemember(id_member) {
    var id = id_member
    // alert(id)
    Swal.fire({
      title: "คุณต้องการลบชอมูลนี้หรือไม?",
      showCancelButton: true,
      confirmButtonText: "ใช่",
      cancelButtonText: "ไม่"
    }).then((result) => {
      if (result.isConfirmed) {
        var settings = {
          "url": "http://localhost/shopbrand/api/delete.php",
          "method": "POST",
          "timeout": 0,
          "headers": {
            "Content-Type": "application/json",
          },
          "data": JSON.stringify({
            "id": id
          }),
        };

        $.ajax(settings).done(function(response) {
          // console.log(response);
          if (response.status == 'DELETE ok') {
            Swal.fire({
                title: "ลบข้อมูลเรียบร้อย",
                text: "",
                showConfirmButton: true,
                icon: "success",
                timer: 800
              })
          }
        });

      }
    });

  }
</script>