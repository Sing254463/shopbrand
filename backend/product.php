<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <!-- head -->
  <?php require_once("template/head.php"); ?>

</head>

<body>
  <!-- navber -->
  <?php require_once("template/navber.php"); ?>
  <!-- container -->
  <div class="dashboard-content px-3 pt-4">
    <div class="container-fluid mt-5">
      <div class="card" style="width: 100%;">
        <div class="card-body">
          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createModal">
            เพิ่มสนค้า
          </button>
          <div class="table-responsive">
            <table id="myTable" class="display table" style="width: 100%;">
              <thead>
                <tr>
                  <th>ลำดับ</th>
                  <th>nameproduct</th>
                  <th>description</th>
                  <th>brand</th>
                  <th>price</th>
                  <th>quantity</th>
                  <th>รูปสินค้า</th>
                  <th>ดูข้อมูล</th>
                  <th>การจัดการ</th>
                </tr>
              </thead>
              <tbody id="showproduct">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- createModal -->
  <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="createModalLabel">เพิ่มสินค้าใหม่</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2">
            <div class="col">
              <label class="form-label">nameproduct</label>
              <input type="text" class="form-control" id="nameproduct">
            </div>
            <div class="col">
              <label class="form-label">description</label>
              <input type="text" class="form-control" id="description">
            </div>
            <div class="col">

              <label class="form-label">namebrand</label>
              <select class="form-select" id="brand" aria-label="Default select example">
                <?php
                $brand = "SELECT * FROM brand WHERE row = 1";
                $result = mysqli_query($conn, $brand);
                echo '<option selected disabled >Open this select menu</option>';
                foreach ($result as $brand) {
                  echo '<option value="' . $brand['id'] . '"> ' . $brand['namebrand'] . '</option>';
                }  ?>
              </select>
            </div>
            <div class="col">
              <label class="form-label">price</label>
              <input type="number" class="form-control" id="price">
            </div>
            <div class="col">
              <label class="form-label">quantity</label>
              <input type="number" class="form-control" id="quantity">
            </div>
            <div class="col">
              <label class="form-label">img_profile</label>
              <input type="file" id="img_profile" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary create">เพิ่มสนค้า</button>
        </div>
      </div>
    </div>
  </div>


  <!-- showdataone -->
  <div class="modal fade" id="showdataone" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2">
            <div class="col">
              <label class="form-label">nameproduct</label>
              <input type="text" disabled class="form-control" id="Showonenameproduct">
            </div>
            <div class="col">
              <label class="form-label">description</label>
              <input type="text" disabled class="form-control" id="Showonedescription">
            </div>
            <div class="col">
              <label class="form-label">namebrand</label>
              <input type="text" disabled class="form-control" id="Showonenamebrand">
            </div>
            <div class="col">
              <label class="form-label">price</label>
              <input type="text" disabled class="form-control" id="Showoneprice">
            </div>
            <div class="col">
              <label class="form-label">quantity</label>
              <input type="text" disabled class="form-control" id="Showonequantity">
            </div>
            <div class="col">
              <label class="form-label">img_profile</label>
              <br>
              <img id="Showoneimg_profile" src="" alt="" style="width: 20%;">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <!-- edut/update product Modal -->
  <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel">แก้ไขสินค้า</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2">
            <div class="col">
              <label class="form-label">nameproduct</label>
              <input type="text" class="form-control" id="edit_nameproduct">
            </div>
            <div class="col">
              <label class="form-label">description</label>
              <input type="text" class="form-control" id="edit_description">
            </div>
            <div class="col">
              <label class="form-label">namebrand</label>
              <select class="form-select" id="edit_brand" aria-label="Default select example">
                <option selected disabled></option>
                <?php
                foreach ($result as $edit_brand) {
                  echo '<option value="' . $edit_brand['id'] . '"> ' . $edit_brand['namebrand'] . '</option>';
                }  ?>
                <!-- <option id="old_brand" hidden>ค่าเดิม</option> -->
              </select>
            </div>
            <div class="col">
              <label class="form-label">price</label>
              <input type="number" class="form-control" id="edit_price">
            </div>
            <div class="col">
              <label class="form-label">quantity</label>
              <input type="number" class="form-control" id="edit_quantity">
            </div>
            <div class="col">
              <label class="form-label">img_profile</label>
              <br>
              <img id="edit_img_profile_show" src="" alt="" style="width: 20%;">
            </div>
            <div class="col">
              <label class="form-label">img_profile</label>
              <input type="file" id="edit_img_profile" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
          <button type="button" class="btn btn-primary" id="editproduct">แก้ไข</button>
        </div>
      </div>
    </div>
  </div>


  <!-- footer -->
  <?php require_once("template/footer.php"); ?>

  <!-- script jquery 3.7.1 -->
  <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

  <!-- script bootstrap 5.3.3 -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

  <!--script datatables 2.0.2 -->
  <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

  <!--script popperjs 2.0.2 -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

  <!-- sweetalert2 -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


</body>

</html>
<script src="dist/js/product/showproduct.js"></script>

<script src="dist\js\product\addproduct.js"></script>
<script src="dist\js\product\showproductone.js"></script>
<script src="dist\js\product\editandupdateproduct.js"></script>
<script>
  function removeproduct(idproduct) {
    var id = idproduct
    Swal.fire({
      title: "คุณต้องการลบชอมูลนี้หรือไม?",
      showCancelButton: true,
      confirmButtonText: "ใช่",
      cancelButtonText: "ไม่"
    }).then((result) => {
      if (result.isConfirmed) {
        var settings = {
          "url": "http://localhost/shopbrand/api/product/delete.php",
          "method": "POST",
          "timeout": 0,
          "headers": {
            "Content-Type": "application/json",
          },
          "data": JSON.stringify({
            "id": id
          }),
        };
        $.ajax(settings).done(function(response) {
          // console.log(response);
          if (response.status == 'DELETE ok') {
            Swal.fire({
                title: "ลบข้อมูลเรียบร้อย",
                text: "",
                showConfirmButton: true,
                icon: "success",
                timer: 800
              })
          }
        });
      }
    });

  }
</script>