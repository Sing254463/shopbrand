<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="your, comma, separated, keywords">
<meta name="description" content="Description of your page">
<meta name="author" content="Your Name">
<meta property="og:title" content="Your Page Title">
<meta property="og:type" content="website">
<meta property="og:url" content="https://yourwebsite.com">
<meta property="og:image" content="https://yourwebsite.com/image.jpg">
<meta property="og:description" content="Description of your page">

<!-- icon title -->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- title -->
<title><?= $title ?></title>
<!-- bootstrap 5.3.3 -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

<!-- icon font-awesome 6.5.1 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">

<!-- bootstrap-icons 1.11.3 -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

<!-- css datatables -->
<link rel="stylesheet" href="https://cdn.datatables.net/2.0.2/css/dataTables.dataTables.min.css">

<!-- css -->
<link rel="stylesheet" href="dist/css/style.css">

<!-- js -->
<!-- <script src="dist/js/script.js"></script> -->

<!-- --------font Kanit------- -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">