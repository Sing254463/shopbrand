<?php
if (!isset($_SESSION['id'])) {
    header('location: index.php');
}
?>

<div class="main-container d-flex">
    <div class="sidebar " id="side_nav">
        <div class="header-box px-2 pt-4 pb-4 d-flex justify-content-between">
            <h1 class="fs-4"><span class="bg-white text-dark rounded shadow px-2 me-2">logo</span> <span class="text-white">หลังบ้าน</span></h1>
            <button class="btn d-md-none d-block close-btn px-1 py-0 text-white"><i class="fa-solid fa-bars"></i></button>
        </div>

        <ul class="list-unstyled px-2">
            <li class=""><a href="dashboard.php" class="text-decoration-none px-3 py-2 d-block"><i class="fa-solid fa-chart-line"></i> Dashboard</a></li>
            <li class=""><a href="member.php" class="text-decoration-none px-3 py-2 d-block"><i class="fa-solid fa-users"></i>
                    Member</a></li>
            <li class=""><a href="product.php" class="text-decoration-none px-3 py-2 d-block"><i class="fa-solid fa-cart-shopping"></i></i> Product</a></li>
        <li class=""><a href="systembrand.php" class="text-decoration-none px-3 py-2 d-block"><i class="fa-solid fa-pen-to-square"></i>
                System and Add Brand</a></li>
        <li class=""><a href="Order.php" class="text-decoration-none px-3 py-2 d-block"><i class="fa-solid fa-clipboard-list"></i>
                รายการ</a></li>
        </ul>
        <hr class="h-color mx-2">





    </div>
    <div class="content">
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid">
                <a class="navbar-brand fs-4" href="#" id="logo"><span class="bg-dark rounded px-2 py-0 text-white">logo</span></a>

                <div class="d-flex justify-content-between d-md-none d-block">
                    <button class="btn px-1 py-0 open-btn me-2"><i class="fa-solid fa-bars"></i></button>
                    <a class="navbar-brand fs-4" href="#"><span class="bg-dark rounded px-2 py-0 text-white">logo</span></a>
                </div>

                <?php
                if (isset($_SESSION['id'])) {


                ?>

                        <button type="button" id="logout" class="btn btn-outline-danger text-dark">
                            ออกจากระบบ
                        </button>
                    <?php
                }
                    ?>
            </div>
        </nav>




        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


     
  
    
 

                <script>
                    $(document).ready(function(){
                        $("#logout").click(function(){
                               Swal.fire({
  title: "ต้องการออกจากระบบหรือไม?",
  icon: "question",
  iconColor: "#FF7F00",
  showCancelButton: true,
  confirmButtonColor: "#d33",
  confirmButtonText: "ออกจากระบบ",
  cancelButtonText:"ยกเลิก"
}).then((result) => {
    if (result.isConfirmed) {
                            $.ajax({
                                method:"post",
                                url:"system/logout.php",
                                data:{
                                    btn_login: true},
                                success:function(status){
                                    var error =JSON.parse(status)
                                    if(error.status == "logout"){
                                        Swal.fire({
      title: "ออกจากระบบสำเร็จ!",
      icon: "success",
      timer: 1500
    }).then(()=>{
        window.location.href = "index.php";
    })
                                    }
                                }
                            })
                         }
                        });
                        })
                    })
                </script>
        <script>
            $(".sidebar ul li").on('click', function() {
                $(".sidebar ul li.active").removeClass('active');
                $(this).addClass('active');
            });

            $('.open-btn').on('click', function() {
                $('.sidebar').addClass('active');

            });


            $('.close-btn').on('click', function() {
                $('.sidebar').removeClass('active');

            })
        </script>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var currentUrl = "<?php echo basename($_SERVER['REQUEST_URI']); ?>";
                var sidebarItems = document.querySelectorAll('.sidebar ul li');

                sidebarItems.forEach(function(item) {
                    var menuUrl = item.querySelector('a').getAttribute('href');
                    var menuFile = menuUrl.substring(menuUrl.lastIndexOf('/') + 1);

                    if (currentUrl === menuFile) {
                        item.classList.add('active');
                    }
                });
            });
        </script>