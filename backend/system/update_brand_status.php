<?php

require_once('../../config/database.php');


// รับค่าจากการ POST
$data = json_decode(file_get_contents("php://input"));

// ตรวจสอบว่ามีข้อมูลที่ส่งมาหรือไม่
if(isset($data->brandId) && isset($data->isChecked)) {
    // อัปเดตค่าในฐานข้อมูล
    $sql = "UPDATE brand SET row = " . ($data->isChecked ? 1 : 0) . " WHERE id = " . $data->brandId;
    $result = $conn->query($sql);

    // ตรวจสอบผลลัพธ์
    if ($result === TRUE) {
        echo json_encode(array("success" => true));
    } else {
        echo json_encode(array("success" => false));
    }
} else {
    // ถ้าข้อมูลที่ส่งมาไม่ถูกต้องหรือไม่ครบถ้วน
    echo json_encode(array("success" => false, "message" => "Invalid data sent"));
}

// ปิดการเชื่อมต่อฐานข้อมูล
$conn->close();
?>