<?php
    require_once("../../config/database.php");
    
    // เป็นการ ใช้ ajax ในการส่งข้อมูล จะส่งข้อมูล แต่ตัวเว็บจะไม่มาหน้านี้
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        session_destroy();
        echo '{"status":"logout"}';
    }


    // แบบเก่าที่ ใช้การส่งแบบ form จะทำการมาไฟล์ logout.php
    // session_destroy();
    // // ทำการ ทำลาย session ที่ทำการเริ่มอยู่
    //     echo "!";
    //     echo "<script src='https://cdn.jsdelivr.net/npm/sweetalert2@10'></script>";
    //     echo "<script>
    //         Swal.fire({
    //             title: 'Logout Success',
    //             text: 'ออกจากระบบสำเร็จ',
    //             icon: 'success',
    //             showCancelButton: false,
    //             }).then((result) => {
    //              if (result.isConfirmed) {
    //                     window.location.href = '../index.php';
    //              }else{
    //                 window.location.href = '../index.php';
    //              }
    //         });
    //     </script>";
    //     // ทำการใช้ sweetalert2 ให้ขึ้นว่า Logout Success ออกจากระบบสำเร็จ
    // exit();  
    // // จบการทำงาน
?>