<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>

</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <!-- container -->
    <div class="dashboard-content px-3 pt-4">
        <div class="container-fluid mt-3">
            <div class="card" style="width: 100%;">
                <div class="card-header">
                    <h1 class="text-center mb-3">Create Member</h1>
                </div>
                <div class="card-body">
                    <!-- <form> -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="username" class="form-label">Username</label>
                                <input type="text" class="form-control" id="username">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="Password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="Password">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="name" class="form-label">Real name</label>
                                <input type="text" class="form-control" id="name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="lname" class="form-label">Last name</label>
                                <input type="text" class="form-control" id="lname">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="Nickname" class="form-label">Nickname</label>
                                <input type="text" class="form-control" id="Nickname">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary Create">create memder</button>
                    <a href="member.php" type="button" class="btn btn-danger">cancel</a>
                    <!-- </form> -->
                </div>
            </div>



        </div>
    </div>


    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



</body>

</html>

<script>
    $('document').ready(function() {
        $(".Create").click(function() {
            var settings = {
                "url": "http://localhost/ทำเล่น/api/create.php",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "Cookie": "PHPSESSID=1j5im5b6loqh3f2ikd5ul9ul3e"
                },
                "data": JSON.stringify({
                    "username": $('#username').val(),
                    "password": $('#Password').val(),
                    "name": $('#name').val(),
                    "lname": $('#lname').val(),
                    "nickname": $('#Nickname').val()
                }),
            };

            $.ajax(settings).done(function(response) {
                // console.log(response);
                if (response.status == 'usernameซ้ำ'){
                    Swal.fire({
                            title: "username ซ้ำ",
                            text: "กรุณาเปลียน username",
                            icon: "error"
                        });
                }
                else if(response.status == 'error'){
                    Swal.fire({
                            title: "ไม่สามารถสมัคสมาชิกได้",
                            text: "กรุณาทำการติดต่อเจ้าหน้าที่",
                            icon: "error"
                        });
                }
                else {
                    $('#username').val(""),
                    $('#Password').val(""),
                    $('#name').val(""),
                    $('#lname').val(""),
                    $('#Nickname').val(""),
                        Swal.fire({
                            title: "สมัครสมาชิกสำเร็จ",
                            text: "",
                            icon: "success"
                        });
                }
            });
        });
    })
</script>