<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");

$brand = "SELECT * FROM brand WHERE row = 1";
$chack = mysqli_query($conn, $brand);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>
</head>
<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <!-- container -->
    <div class="dashboard-content px-3 pt-4">
        <div class="container-fluid mt-5">
            <div class="card" style="width: 100%;">
                <div class="card-header">
                    <h1 class="text-center">เพิ่มสินค้า</h1>
                </div>
                <div class="card-body">
                    <!-- เพิ่มสินค้า -->
                    <div class="row row-cols-1 row-cols-sm-1 row-cols-md-2 g-3">
                        <div class="mb-3">
                            <div class="col">
                                <label class="form-label">ชื่อสินค้า</label>
                                <input type="text" id="nameproduct" class="form-control" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">รายละเอียดสินค้า</label>
                                <input type="text" id="Product_details" class="form-control" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">ราคาสินค้า</label>
                                <input type="number" id="price" class="form-control" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">brand สินค้า</label>
                                <select class="form-select" id="brand" required>
                                    <option selected disabled>เลือก brand สินค้า</option>
                                    <?php while ($row_modal = mysqli_fetch_assoc($chack)) { ?>
                                        <option value="<?= $row_modal['id'] ?>"><?= $row_modal['namebrand'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">จำนวนสินค้า</label>
                                <input type="number" class="form-control" id="quantity" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="formFile" class="form-label">รูปสินค้า</label>
                                <input class="form-control" type="file" id="formFile" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-end">
                    <button type="button" class="btn btn-success" onclick="addproduct()">เพิ่มสินค้า</button>
                    <button type="reset" class="btn btn-danger">ยกเลิก</button>
                </div>
                </div>
            </div>
            </div>
            <!-- footer -->
            <?php require_once("template/footer.php"); ?>
            <!-- script jquery 3.7.1 -->
            <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
            <!-- script bootstrap 5.3.3 -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
            <!--script datatables 2.0.2 -->
            <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>
            <!--script popperjs 2.0.2 -->
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>
            <!-- sweetalert2 -->
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
            <!-- addproduct.js -->
            <script src="dist/js/addproduct.js"></script>
</body>
</html>