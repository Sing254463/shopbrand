<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>

</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <!-- container -->
    <div class="dashboard-content px-3 pt-4">
        <div class="container-fluid mt-3">
            <div class="card" style="width: 100%;">
                <div class="card-header">
                    <h1 class="text-center mb-3">Edit Member</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="name" class="form-label">Real name</label>
                                <input type="text" class="form-control" id="name" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="lname" class="form-label">Last name</label>
                                <input type="text" class="form-control" id="lname" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="Nickname" class="form-label">Nickname</label>
                                <input type="text" class="form-control" id="Nickname" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="tel" class="form-label">tel</label>
                                <input type="text" class="form-control" id="tel" disabled>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="address" class="form-label">address</label>
                                <input type="text" class="form-control" id="address" disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="sub_district" class="form-label">sub_district</label>
                                <input type="text" class="form-control" id="sub_district" disabled>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="district" class="form-label">district</label>
                                <input type="text" class="form-control" id="district" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="province" class="form-label">province</label>
                                <input type="text" class="form-control" id="province" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="mb-3">
                                <label for="postcode" class="form-label">postcode</label>
                                <input type="text" class="form-control" id="postcode" disabled>
                            </div>
                        </div>
                    </div>
                    <a href="member.php" type="button" class="btn btn-danger">cancel</a>
                </div>
            </div>



        </div>
    </div>


    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



</body>

</html>

<script>
    const params = new URLSearchParams(window.location.search);
    const id = params.get('id');

    var settings = {
        "url": "http://localhost/ทำเล่น/api/readone.php?id=" + id,
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Cookie": "PHPSESSID=1j5im5b6loqh3f2ikd5ul9ul3e"
        },
    };

    $.ajax(settings).done(function(response) {
        // console.log(response);
        //   var jsonobj = JSON.parse(response);
        $('#name').val(response[0].name);
        $('#lname').val(response[0].lname);
        $('#Nickname').val(response[0].nickname);
        $('#tel').val(response[0].tel);
        $('#address').val(response[0].address);
        $('#sub_district').val(response[0].sub_district);
        $('#district').val(response[0].district);
        $('#province').val(response[0].province);
        $('#postcode').val(response[0].postcode);
    });
</script>