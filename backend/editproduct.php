<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>

</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <!-- container -->
    <div class="dashboard-content px-3 pt-4">
    <div class="container-fluid mt-5">
    <div class="card" style="width: 100%;">
    
</div>

    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



</body>

</html>