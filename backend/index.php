<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>

</head>

<body>
    <!-- container -->
    <div class="dashboard-content px-3 pt-4">
        <div class="container-fluid mt-5">

            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 d-flex justify-content-center ">
                    <div class="card text-center" id="login" style="width: 35rem;">
                        <div class="container my-3">
                            <h1>เข้าสู่ระบบ หลังบ้าน</h1>
                        </div>
                        <hr>
                        <div class="card-body text-start ">
                            <!-- <form action="system/login.php" method="post"> -->
                            <div class="container mb-3">
                                <div class="mb-3">
                                    <label for="exampleInputusername" class="form-label">ชื่อผู้ใช้</label>
                                    <input type="text" class="form-control login" name="username" id="username">
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputpassword" class="form-label">รหัสผ่าน</label>
                                    <input type="password" class="form-control login" name="password" id="password">
                                </div>

                                <div class="mt-5">
                                    <button type="submit" name="btn_login" id="btn_login" class="btn btn-primary">เข้าสู่ระบบ</button>
                                </div>
                            </div>
                            <!-- </form> -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>


        </div>
    </div>


    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function() {
            $("#btn_login").click(function() {
                var login = true;
                if ($("#username").val().length <= 0) {
                    login = false;
                    Swal.fire({
                        title: "ไม่สามารถเข้าสู่ระบบได้",
                        text: "ใส่ username",
                        icon: "error"
                    });

                } else if ($("#password").val().length <= 0) {
                    login = false
                    Swal.fire({
                        title: "ไม่สามารถเข้าสู่ระบบได้",
                        text: "ใส่ password",
                        icon: "error"
                    });
                }
                $.ajax({
                    method: "post",
                    data: {
                        username: $("#username").val(),
                        password: $("#password").val()
                    },
                    url: "system/login.php",
                    success: function(status) {
                        var error = JSON.parse(status)
                        if (error.status == 404) {
                            Swal.fire({
                                title: "ไม่สามารถเข้าสู่ระบบได้",
                                text: "username หรือ password ไม่ถูกต้อง",
                                icon: "error"
                            });
                        } else {
                            Swal.fire({
                                title: "เข้าสู่ระบบสำเร็จ",
                                text: "ยินดีต้อนรับสู่ระบบหลังบ้าน",
                                icon: "success"
                            }).then(() => {
                                window.location.href = "dashboard.php";
                            })
                        }
                    }
                })
            });
        });
    </script>


</body>

</html>