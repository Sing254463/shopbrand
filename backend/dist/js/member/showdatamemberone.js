var memberId;
  // กำหนด ตัวแปล memberId

  $(document).on('click', '.showdatamember', function() {
    // ใช้ on เช็ค ว่า มีการ click ของ class showdatamember หรือไม
    memberId = $(this).data('member-id');
    // ทำการ ให้ memberId นั้นเท่ากับ button class showdatamember โดย เอา data-member-id ว่ามีค่าอะไร ใส่เข้าไปใน memberId
    // console.log("memberID: " + memberId);
    // ซึ่่ง data-member-id จะทำการเก็บ user.id_member เอาไว้ ว่าเป็น id อะไร


    // ทำการดึงข้อมูลสมาชิกจาก API โดยใช้ ID ของสมาชิกนั้น
    var settings = {
      "url": "http://localhost/ทำเล่น/api/readone.php?id=" + memberId,
      "method": "GET",
      "timeout": 0,
      
    };

    $.ajax(settings).done(function(response) {
      // ทำการกำหนดค่าให้กับฟิลด์ของ Modal
      $('#name').val(response[0].name);
      $('#lname').val(response[0].lname);
      $('#Nickname').val(response[0].nickname);
      $('#tel').val(response[0].tel);
      $('#address').val(response[0].address);
      $('#sub_district').val(response[0].sub_district);
      $('#district').val(response[0].district);
      $('#province').val(response[0].province);
      $('#postcode').val(response[0].postcode);
    });
  });