// เป็นการสร้าง member แบบ API
    $('document').ready(function() {
        $(".Create").click(function() {
          var Register = true;

if($('#username').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ ชื่อผู้ใช้",
            icon: "error",
        });
}
    else if($('#Password').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ password",
            icon: "error",
        });
}
    else if($('#createname').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ ชื่อ",
            icon: "error",
        });
}
    else if($('#createlname').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ ชื่อนามสกุล",
            icon: "error",
        });
}
    else if($('#createNickname').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ ชื่อเล่น",
            icon: "error",
        });
}
if(Register){
            var settings = {
                "url": "http://localhost/ทำเล่น/api/create.php",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                    "Cookie": "PHPSESSID=1j5im5b6loqh3f2ikd5ul9ul3e"
                },
                "data": JSON.stringify({
                    "username": $('#username').val(),
                    "password": $('#Password').val(),
                    "name": $('#createname').val(),
                    "lname": $('#createlname').val(),
                    "nickname": $('#createNickname').val()
                }),
            };

            $.ajax(settings).done(function(response) {
                if (response.status == 'usernameซ้ำ'){
                    Swal.fire({
                            title: "username ซ้ำ",
                            text: "กรุณาเปลียน username",
                            icon: "error"
                        });
                }
                else if(response.status == 'error'){
                    Swal.fire({
                            title: "ไม่สามารถสมัคสมาชิกได้",
                            text: "กรุณาทำการติดต่อเจ้าหน้าที่",
                            icon: "error"
                        });
                }
                else {
                    $('#username').val(""),
                    $('#Password').val(""),
                    $('#name').val(""),
                    $('#lname').val(""),
                    $('#Nickname').val(""),
                        Swal.fire({
                            title: "สมัครสมาชิกสำเร็จ",
                            text: "",
                            icon: "success"
                        })
                        .then(() => {
              window.location.href = 'member.php';
            });
                }
            });
        };
        });
        });