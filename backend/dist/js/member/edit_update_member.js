var id;

// ทำการเพิ่มคลาส .edit ด้วยเหตุผลที่ไม่ชัดเจน
$(document).on('click', '.editmember', function() {
  id = $(this).data('member-id');
  // console.log("memberID: " + id);

  // ทำการดึงข้อมูลสมาชิกจาก API โดยใช้ ID ของสมาชิกนั้น
  // โชข้อมูลเก่า
  var settings = {
    "url": "http://localhost/ทำเล่น/api/readone.php?id=" + id,
    "method": "GET",
    "timeout": 0,
    
  };

  $.ajax(settings).done(function(response) {
    $('#editname').val(response[0].name);
    $('#editlname').val(response[0].lname);
    $('#editNickname').val(response[0].nickname);
    $('#edittel').val(response[0].tel);
    $('#editaddress').val(response[0].address);
    $('#editsub_district').val(response[0].sub_district);
    $('#editdistrict').val(response[0].district);
    $('#editprovince').val(response[0].province);
    $('#editpostcode').val(response[0].postcode);
  });

  // ทำการส่งข้อมูลใหม่ ไปแก้ไข
  $('document').ready(function() {
    $(".edit").click(function() {
      var settings = {
        "url": "http://localhost/ทำเล่น/api/update.php",
        "method": "PATCH",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json",
        },
        "data": JSON.stringify({
          "name": $('#editname').val(),
          "lname": $('#editlname').val(),
          "nickname": $('#editNickname').val(),
          "tel": $('#edittel').val(),
          "address": $('#editaddress').val(),
          "sub_district": $('#editsub_district').val(),
          "district": $('#editdistrict').val(),
          "province": $('#editprovince').val(),
          "postcode": $('#editpostcode').val(),
          "id": id
        }),
      };

      $.ajax(settings).done(function(response) {
        // console.log(response);
        if (response.status == 'updatemember error') {
          Swal.fire({
            title: "เกิดขอผิดพลาด",
            text: "กรุณาทำการติดต่อเจ้าหน้าที่",
            icon: "error"
          });
        } else {
          Swal.fire({
            title: "สำเร็จ",
            text: "ทำการแก้ไขข้อมูลสำเร็จ",
            icon: "success"
          })

        }
      });
    });
  });
});