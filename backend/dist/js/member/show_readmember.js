//  การดึงข้อมูล API ทั้งหมด มา โช โดยทำการ add เข้า DataTable 
var table;

        function loadXMLDoc() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var response = JSON.parse(this.responseText);

                    // Clear the table body
                    $('#showmember').empty();
                    var i = 0;
                    // Append new rows
                    for (let user of response) {
i++;
                        var row = `
                            <tr>
                                <td>${i}</td>
                                <td>${user.id_member}</td>
                                <td>${user.username}</td>
                                <td>${user.name}</td>
                                <td>${user.lname}</td>
                                <td>${user.nickname}</td>
                                <td>${user.tel}</td>
                                <td>
                                    <button type="button" class="btn btn-primary showdatamember" data-member-id="${user.id_member}" data-bs-toggle="modal" data-bs-target="#showmemberModal">
                                        ข้อมูลเพิ่มเติม
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary editmember" data-member-id="${user.id_member}" data-bs-toggle="modal" data-bs-target="#editmemberModal">
                                        แก้ไข
                                    </button>
                                    <button class="btn btn-danger" type="button" onclick="removemember(${user.id_member})">ลบ</button>
                                </td>
                            </tr>`;
                        $('#showmember').append(row);
                    }

                    
                    var currentPage = table.page();
                    // เป็นการบันทึกหน้าที่ทำการอยู่เอาไว้ เพิ่มไม่ให้ ทำการโหลดกลับไปหน้า แรก

                    table.clear().rows.add($('#showmember tr')).draw();
                    // เป็นการอัพเดสหรือล้างข้อมูลที่ทำการโชเอาไว้

                    table.page(currentPage).draw('page');
                    //ทำการตั้งค่าหน้าปัจจุบันของ DataTable กลับไปยังหน้าที่บันทึกไว้
                }
            };
            xhttp.open("GET", "http://localhost/ทำเล่น/api/read.php", true);
            xhttp.send();
        }
        $(document).ready(function() {
            table = $('#myTable').DataTable();
            loadXMLDoc();
            setInterval(loadXMLDoc, 3000);
        });