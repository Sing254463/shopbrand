//  การดึงข้อมูล API ทั้งหมด มา โช โดยทำการ add เข้า DataTable 
var table;

function loadXMLDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);

            // Clear the table body
            $('#showproduct').empty();
            var i = 0;
            // Append new rows
            for (let product of response) {
                i++;
                var trimmedDescription = product.description.length > 20 ? product.description.substring(0, 20) + '...' : product.description;
                var row = `
                    <tr>
                        <td>${i}</td>
                        <td>${product.nameproduct}</td>
                        <td>${trimmedDescription}</td>
                        <td>${product.brand}</td>
                        <td>${product.price}</td>
                        <td>${product.quantity}</td>
                        <td><img src="imgproduct/${product.img_profile}" alt="" style=" width:35%;"></td>
                        <td>
                            <button type="button" class="btn btn-primary mx-1 showdataone" data-products-id="${product.idproduct}" data-bs-toggle="modal" data-bs-target="#showdataone">ดูข้อมูลสินค้า</button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary mx-1 editproduct" data-products-id="${product.idproduct}" data-bs-toggle="modal" data-bs-target="#edit">แก้ไข</button>
                            <button class="btn btn-danger" type="button" onclick="removeproduct(${product.idproduct})">ลบ</button>
                        </td>
                    </tr>`;
                $('#showproduct').append(row);
            }

            var currentPage = table.page();
            // Clear existing table data and add new rows
            table.clear().rows.add($('#showproduct tr')).draw();

            // Set back to the current page
            table.page(currentPage).draw('page');
        }
    };
    xhttp.open("GET", "http://localhost/shopbrand/api/product/read.php", true);
    xhttp.send();
}


        
        $(document).ready(function() {
            table = $('#myTable').DataTable();
            loadXMLDoc();
            setInterval(loadXMLDoc, 5000);
        });