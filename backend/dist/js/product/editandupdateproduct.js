  var idproduct;
  $(document).on('click', '.editproduct', function() {
    idproduct = $(this).data('products-id');
    // console.log("product-id" + idproduct);
    // โชขูมมูลเก่า
    var settings = {
      "url": "http://localhost/shopbrand/api/product/readone.php?id=" + idproduct,
      "method": "GET",
      "timeout": 0,
    };
    $.ajax(settings).done(function(response) {
      $('#edit_nameproduct').val(response[0].nameproduct);
      $('#edit_description').val(response[0].description);
      $('#showbrand').text(response[0].brand);
      $('#edit_brand').val(response[0].id_brand);
      $('#edit_price').val(response[0].price);
      $('#edit_quantity').val(response[0].quantity);
      $('#edit_img_profile_show').attr('src', "imgproduct/" + response[0].img_profile).val(response[0].img_profile);
    });
    // ยังทำไม่เสร็จ ต้องแก้ปัญหา ที่ต้องแก้ เกี่ยวกับรูป กับ brand ที่ไม่แสดง
    $(document).ready(function() {
      $("#editproduct").click(function() {

        var brand = $("#edit_brand").val();
        var nameproduct = $('#edit_nameproduct').val();
        var productDetails = $("#edit_description").val();
        var price = $("#edit_price").val();
        var quantity = $("#edit_quantity").val();
        var imageFile = $('#edit_img_profile')[0].files[0];
        // ทำการเช็คว่า มีการอัพโหลดรูปมาไม ถ้าไม่มี จะทำการ เอา value เก่า มาใส่แทน
        if(!imageFile){
          imageFile = $('#edit_img_profile_show').val()
        }
        var form = new FormData();
        form.append("nameproduct", nameproduct);
        form.append("Product_details", productDetails);
        form.append("brand", brand);
        form.append("price", price);
        form.append("quantity", quantity);
        form.append("imageFile", imageFile);
        form.append("id", idproduct);

        var settings = {
          "url": "http://localhost/shopbrand/api/product/update.php",
          "method": "POST",
          "timeout": 0,
          "processData": false,
          "mimeType": "multipart/form-data",
          "contentType": false,
          "data": form
        };
        $.ajax(settings).done(function (response) {
          if (response.status == 'updatemember error') {
                  Swal.fire({
                    title: "เกิดขอผิดพลาด",
                    text: "กรุณาทำการติดต่อเจ้าหน้าที่",
                    icon: "error"
                  });
                }else {
                  Swal.fire({
                    title: "สำเร็จ",
                    text: "ทำการแก้ไขข้อมูลสำเร็จ",
                    icon: "success"
                  })
                  $('#edit_img_profile').val("")
            }
        });
      })
    });
  });
