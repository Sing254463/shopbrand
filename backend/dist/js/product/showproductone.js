var idproduct;
$(document).on('click', '.showdataone', function() {
  idproduct = $(this).data('products-id');
  // console.log("product-id" + idproduct);
  // โชขูมมูลเก่า
  var settings = {
    "url": "http://localhost/shopbrand/api/product/readone.php?id=" + idproduct,
    "method": "GET",
    "timeout": 0,
  };
  $.ajax(settings).done(function(response) {
    // ทำการกำหนดค่าให้กับฟิลด์ของ Modal
    // console.log(response);
    $('#Showonenameproduct').val(response[0].nameproduct);
    $('#Showonedescription').val(response[0].description);
    $('#Showonenamebrand').val(response[0].brand);
    $('#Showoneprice').val(response[0].price);
    $('#Showonequantity').val(response[0].quantity);
    $('#Showoneimg_profile').attr('src', "imgproduct/" + response[0].img_profile);
  });
  });