$(document).ready(function() {
    $(".create").click(function() {
      var addproduct = true;
      if ($("#nameproduct").val().length <= 0) {
        addproduct = false;
        Swal.fire({
          title: "ไม่สามารถเพิ่มสินค้าใหม่ได้",
          text: "กรุณาใส่ ชื่อสินค้า",
          icon: "error",
        });
      } else if ($("#brand").val() == null) {
        addproduct = false;
        Swal.fire({
          title: "ไม่สามารถเพิ่มสินค้าใหม่ได้",
          text: "กรุณาเลือก brand",
          icon: "error",
        });
      } else if ($("#price").val().length <= 0 || $("#price").val() <= 0) {
        addproduct = false;
        Swal.fire({
          title: "ไม่สามารถเพิ่มสินค้าใหม่ได้",
          text: "กรุณาใส่ price",
          icon: "error",
        });
      }
      else if ($("#quantity").val().length <= 0) {
        addproduct = false;
        Swal.fire({
          title: "ไม่สามารถเพิ่มสินค้าใหม่ได้",
          text: "กรุณาใส่ จำนวนสินค้า",
          icon: "error",
        });
      }
if (addproduct) {
        var formData = new FormData(); // ออบเจ็กต์พิเศษที่ใช้สำหรับการสร้างชุดข้อมูล 
        // เก็บข้อมูลในรูปแบบคู่คีย์-ค่า (key-value pairs) 
        var imageFile = $('#img_profile')[0].files[0];//เป็นเอาค่าตัวแรก และ ไฟล์ตัว แรก มาเก็บใส่ตัวแปล imageFile
        formData.append('imageFile', imageFile);
        formData.append('nameproduct', $('#nameproduct').val());
        formData.append('Product_details', $("#Product_details").val());
        formData.append('price', $("#price").val());
        formData.append('brand', $("#brand").val());
        formData.append('quantity', $("#quantity").val());
        var settings = {
            "url": "http://localhost/shopbrand/api/product/addproduct.php",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Cookie": "PHPSESSID=1ci4ffgq0pvg5bknedj4mnsl0g"
              },
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": formData
          };

        $.ajax(settings).done(function (response) {
            console.log(response);
            var response = JSON.parse(response)
            if(response.status == '404'){
                Swal.fire({
                    title: "ไม่สามารถทำการเพิ่มสินค้าได้",
                    text: "มี สินค้านี้ ข้อมูลแล้ว",
                    icon: "error",
                });
                
            }else if(response.status == 'error_addproduct'){
                Swal.fire({
                    title: "ไม่สามารถทำการเพิ่มสินค้าได้",
                    text: "กรุณาใส่รูป",
                    icon: "error",
                });
            }else if(response == "success"){
                $('#nameproduct').val(''),
                $("#Product_details").val(''),
                $("#price").val(''),
                $("#brand").val(''),
                $("#quantity").val(''),
                $("#imageFile").val(''),
                Swal.fire({
                    title: "ทำการเพิ่มสินค้าเสร็จสิ้น",
                    icon: "success"
                });
            }
          });

}
});
});
    // $.ajax({
    //     method:'post',
    //     url:"system/addproduct.php",
    //     data:formData,
    //     contentType: false, // ไม่ตั้งค่า Content-Type
    //     processData: false, // ไม่แปลงข้อมูลเป็น query string
    //     success:function(status){
    //         var error = JSON.parse(status)
    //         if(error.status === '404'){
    //             Swal.fire({
    //                 title: "ไม่สามารถทำการเพิ่มสินค้าได้",
    //                 text: "มี สินค้านี้ ข้อมูลแล้ว",
    //                 icon: "error",
    //             });
    //         }else if(error.status === 'error_addproduct'){
    //             Swal.fire({
    //                 title: "ไม่สามารถทำการเพิ่มสินค้าได้",
    //                 text: "กรุณาติดต่อเจ้าหน้าที่",
    //                 icon: "error",
    //             });
    //         }else{
    //             $('#nameproduct').val(''),
    //             $("#Product_details").val(''),
    //             $("#price").val(''),
    //             $("#brand").val(''),
    //             $("#quantity").val(''),
    //             $("#imageFile").val(''),
    //             Swal.fire({
    //                 title: "ทำการเพิ่มสินค้าเสร็จสิ้น",
    //                 icon: "success"
    //             });
    //         }
    //     }
    // })