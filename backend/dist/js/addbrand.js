function addbrand() {
  var addbrand = true;
  if($("#namebrand").val().length <= 0) {
    var addproduct = false;
    Swal.fire({
      title: "ไม่สามารถทำการเพิ่มBrandสินค้าได้",
      text: "กรุณาทำการกรอก ชื่อBrand",
      showConfirmButton: false,
      icon: "error",
    });
  }else if(addbrand) {
    $.ajax({
      method: "post",
      url: "system/addbrand.php",
      data: {
        namebrand: $("#namebrand").val(),
      },
      success: function (status) {
        var error = JSON.parse(status);
        if (error.status === "404") {
          Swal.fire({
            title: "ไม่สามารถทำการเพิ่ม Brand สินค้าได้",
            text: "มี Brand สินค้า นี้อยู่แล้ว",
            icon: "error",
          });
        }else if (error.status === "405") {
            Swal.fire({
              title: "ไม่สามารถทำการเพิ่มBrandสินค้าได้",
              text: "กรุณาทำการติดต่อเจ้าหน้าที่",
              icon: "error",
            });
        } else {
          $("#namebrand").val(""),
            Swal.fire({
              title: "ทำการเพิ่ม Brand สินค้าเสร็จสิ้น",
              icon: "success",
            }).then(()=>{
                window.location.href = 'systembrand.php';})


        }
      },
    });
  }
}
