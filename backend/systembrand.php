<?php
// config
require_once("../config/config.php");
//  database
require_once("../config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>

</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <!-- container -->
    <div class="dashboard-content px-3 pt-4">
        <div class="container-fluid mt-5">
            <div class="card" style="width: 100%;">
                <div class="card-header text-center">
                    <h1>ตั่งทำ brand</h1>
                </div>
                <div class="container my-3">
                    <div class="row row-cols-1 row-cols-sm-1 row-cols-md-5">

                        <?php
                        $sql = "SELECT * FROM brand";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // แสดงรายการของแบรนด์
                            while ($row = $result->fetch_assoc()) {
                                // กำหนดค่าในตัวแปร isChecked ตามค่าในฐานข้อมูล
                                $isChecked = $row['row'] == 1 ? 'checked' : '';

                                echo '<div class="col">';
                                echo '<div class="form-check form-switch">';
                                echo '<input class="form-check-input" type="checkbox" role="switch" id="brandSwitch' . $row['id'] . '" ' . $isChecked . ' onchange="toggleBrandStatus(' . $row['id'] . ')">';
                                echo '<label class="form-check-label" for="brandSwitch' . $row['id'] . '">' . $row['namebrand'] . '</label>';
                                echo '</div>';
                                echo '</div>';
                            }
                        } else {
                            echo "No brands found";
                        }
                        ?>
                    </div>
                </div>
                <hr>
                <div class="card-header text-center">
                    <h1>เพิ่ม brand</h1>
                </div>
                <div class="container my-3">
                    <div class="row">
                        <div class="col col-sm">
                            <div class="mb-3">
                                <label class="form-label">ชื่อ Brand</label>
                                <input type="text" id="namebrand" class="form-control">
                            </div>
                        </div>
                        <div class="col col-sm mt-3 d-flex align-items-center gap-2">
                            <button type="button" class="btn btn-success" onclick="addbrand()">เพิ่ม</button>  
                            <button type="reset" class="btn btn-danger">ยกเลิก</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        function toggleBrandStatus(brandId) {
            var isChecked = document.getElementById("brandSwitch" + brandId).checked;
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "system/update_brand_status.php", true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    // เซิร์ฟเวอร์ตอบกลับด้วยข้อมูลที่อัปเดตแล้ว
                    var response = JSON.parse(xhr.responseText);
                    if (response.success) {
                        console.log("Brand status updated successfully");
                    } else {
                        console.error("Failed to update brand status");
                    }
                }
            };
            xhr.send(JSON.stringify({
                brandId: brandId,
                isChecked: isChecked
            }));
        }
    </script>

    <script src="dist/js/addbrand.js"></script>

</body>

</html>