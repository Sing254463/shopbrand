<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container">
    <a class="navbar-brand" href="index.php" style="font-size: 30px;" >Logo</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0" style="font-size: 18px;" >
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">หน้าแรก</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="all_products.php">สินค้าทั้งหมด</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            หมวดหมู่สินค้า
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">หมวก</a></li>
            <li><a class="dropdown-item" href="#">เสื้อ</a></li>
            <li><a class="dropdown-item" href="#">กางเกง</a></li>
            <li><a class="dropdown-item" href="#">รองเท่า</a></li>
            <li><a class="dropdown-item" href="#">ถุงเท้า</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">กระเป๋า</a></li>
            <li><a class="dropdown-item" href="#">กระเป๋าตัง</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">ติดต่อร้านค้า</a>
        </li>
      </ul>
      <?php
       if(isset($_SESSION['id_member'])){
         ?>
         <?php
          $pull_data = "SELECT * FROM member WHERE id_member = '".$_SESSION['id_member']."'  ";
          $result_check = mysqli_query($conn, $pull_data);
          $user = mysqli_fetch_assoc($result_check);
?>
<div class="dropdown">
  <button class="btn btn-outline-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
  <?= $user['username']?>
  </button>
  <ul class="dropdown-menu text-center">
    <li><a class="dropdown-item" href="profile.php"><?= $user['username']?></a></li>
    <hr>
    <li><a class="dropdown-item" href="#">แก้ไขข้อมูล</a></li>
    <li> 
      <!-- <form action="system/logout.php" method="post"> -->
               <button type="button" id="logout" class="btn btn-danger text-dark">
      ออกจากระบบ
</button>
<!-- </form> -->
</li>
  </ul>
</div>
        <?php
       }else{
        ?>
          <a href="login_register.php" class="btn btn-outline-success" type="submit">เข้าสู่ระบบ</a>
          <?php
       }
       ?>
    </div>
  </div>
</nav></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


     
  
    
 

<script>
    $(document).ready(function(){
        $("#logout").click(function(){
               Swal.fire({
title: "ต้องการออกจากระบบหรือไม?",
icon: "question",
iconColor: "#FF7F00",
showCancelButton: true,
confirmButtonColor: "#d33",
confirmButtonText: "ออกจากระบบ",
cancelButtonText:"ยกเลิก"
}).then((result) => {
if (result.isConfirmed) {
            $.ajax({
                method:"post",
                url:"system/logout.php",
                data:{
                    btn_login: true},
                success:function(status){
                    var error =JSON.parse(status)
                    if(error.status == "logout"){
                        Swal.fire({
title: "ออกจากระบบสำเร็จ!",
icon: "success",
timer: 1500
}).then(()=>{
window.location.href = "index.php";
})
                    }
                }
            })
         }
        });
        })
    })
</script>

