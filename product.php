<?php
// config
require_once("config/config.php");
//  database
require_once("config/database.php");

$id = $_GET["id"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>
</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <div class="container my-5">
        
    <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-col-mb-2">
            <?php
            $product = "SELECT * FROM products as pro INNER JOIN brand as bra ON pro.brand = bra.id WHERE idproduct = '$id'";
            $chack = mysqli_query($conn , $product);
            $show = mysqli_fetch_array($chack);
            ?>
            <div class="col">
                <img src="backend/imgproduct/<?= $show["img_profile"]?>" alt="<?= $show["img_profile"]?>" style="width: 100%;" >
            </div>
            <div class="col">
            <div class="card">
  <div class="card-body">
    <h5 class="nameproduct mt-3">ชื่อสินค้า : <?= $show["nameproduct"]?></h5>
    <p class="beand mt-3">แบรนด์ : <?= $show["namebrand"]?></p>
    <p class="description mt-3">รายละเอียดสินค้า : <?= $show["description"]?></p>
    <h5 class="price mt-3">ราคา : <i class="bi bi-currency-bitcoin"></i> <?= $show["price"]?> </h5>
    <a href="buy.php?id=<?= $id ?>" class="btn btn-success" >ซื้อ</a>
  </div>
</div>
            </div>

        </div>
    </div>



    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

   <!-- script jquery 3.7.1 -->
   <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

<!-- script bootstrap 5.3.3 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<!--script datatables 2.0.2 -->
<script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

<!--script popperjs 2.0.2 -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

<!-- sweetalert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>