<?php
// config
require_once("config/config.php");
//  database
require_once("config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>
    <script src="dist/js/login_register.js"></script>

</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <!-- container -->
    <div class="container-fluid mt-5">

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 d-flex justify-content-center ">
                <div class="card text-center" id="login" style="width: 35rem;">
                    <div class="container my-3">
                        <h1>เข้าสู่ระบบ</h1>
                    </div>
                    <hr>
                    <div class="card-body text-start ">
                        <div class="container mb-3">
                            <!-- <form action="system/login_register.php" method="post"> -->
                            <!-- เป็นการส่งข้อมูล แบบ form โดยมี method เป็น post โดยทำการส่งข้อมูล และไปหน้า system/login_register.php -->
                            <div class="mb-3">
                                <label for="exampleInputusername" class="form-label">ชื่อผู้ใช้</label>
                                <input type="text" class="form-control login" name="username" id="username">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputpassword" class="form-label">รหัสผ่าน</label>
                                <input type="password" class="form-control login" name="password" id="password">
                            </div>

                            <div class="mt-5">
                                <button type="submit" name="btn_login" id="btn_login" class="btn btn-success" onclick="login()">เข้าสู่ระบบ</button>
                                <!-- onclick="register()" เป็นการ กำหนด อีเว้น ว่ามีการกดหรือไม ถ้ามี จะทำการเรียกใช้ ฟังชั่น login จากไฟล์ login_register.js-->
                                <button type="button" id="login_register" class="btn btn-secondary btn-button">สมัครสมาชิก</button>
                            </div>
                        </div>
                        <!-- </form> -->
                    </div>
                </div>

                <!-- register -->
                <div class="card text-center" id="register" style="width: 35rem; display:none;">
                    <div class="container my-3">
                        <h1>สมัครสมาชิก</h1>
                    </div>
                    <hr>
                    <div class="card-body text-start ">
                        <div class="container mb-3">
                            <!-- <form action="system/login_register.php" method="post"> -->
                            <!-- เป็นการส่งข้อมูล แบบ form โดยมี method เป็น post โดยทำการส่งข้อมูล และไปหน้า system/login_register.php -->

                            <div class="mb-3">
                                <label for="exampleInputusername" class="form-label">ชื่อผู้ใช้</label>
                                <input type="text" class="form-control login" name="register_username" id="register_username" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputpassword" class="form-label">รหัสผ่าน</label>
                                <input type="password" class="form-control login" name="password1" id="password1" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputpassword" class="form-label">ยืนยันรหัสผ่าน</label>
                                <input type="password" class="form-control login" name="password2" id="password2" required>
                            </div>
                            <div class="mt-5">
                                <button type="submit" name="btn_register" class="btn btn-success" onclick="register()">สมัครสมาชิก</button>
                                <!-- onclick="register()" เป็นการ กำหนด อีเว้น ว่ามีการกดหรือไม ถ้ามี จะทำการเรียกใช้ ฟังชั่น register จากไฟล์ login_register.js-->
                                <button type="button" id="login_register" class="btn btn-primary btn-button">เข้าสู่ระบบ</button>

                            </div>

                        </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
            <div class="col-sm-3"></div>
        </div>


    </div>

    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

    <!-- script jquery 3.7.1 -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <!-- script bootstrap 5.3.3 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!--script datatables 2.0.2 -->
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

    <!--script popperjs 2.0.2 -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $('document').ready(function() {

            $(".btn-button").click(function() {
                $('#login').slideToggle('slow');
                $('#register').slideToggle('slow');
            });
        });

        // ทำการกำหนด document.ready (function()
        // โดยมีการทำงานคือ เก็บค่า class btn-button ว่ามีการ click หรือไม ถ้ามีการ click
        // จะให้ทำขั้นตอนใน function คือ id login และ register ทำการ toggle หรือ แสดง และซ่อน
        // ถ้าอันไหน เป็นซ่อน จะแสดง ถ้าอันไหน เป็นแสดง จะทำการ 
    </script>


</body>

</html>