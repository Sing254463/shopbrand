<?php
require_once("../config/database.php");
header('Access-Control-Allow-Origin: *');
//  เพื่ออนุญาตให้เว็บไซต์อื่นสามารถเข้าถึงข้อมูลผ่าน API ของคุณ โดย * หมายถึงทุกๆ โดเมน
header("Content-type: application/json; charset=utf-8");
// ระบุว่าข้อมูลที่ส่งกลับจะเป็นประเภท JSON และใช้การเข้ารหัสเป็น UTF-8 เพื่อให้สามารถอ่านข้อมูลภาษาอื่นๆ ที่มีตัวอักษรพิเศษได้ถูกต้อง

$data = json_decode(file_get_contents("php://input"));

if($_SERVER['REQUEST_METHOD']!== 'POST'){
    echo json_encode(array("status" => "error"));
    die();
}

// $username = $data->username;
// $password = $data->password;
$name = $data->name;
$lname = $data->lname;
$nickname = $data->nickname;
$tel = $data->tel;
$address = $data->address;
$sub_district = $data->sub_district;
$district = $data->district;
$province = $data->province;
$postcode = $data->postcode;
$id = $data->id;

if (strlen($tel) == 9) {
    $tel = "0" . $tel;
}
$updatemember = "UPDATE member SET
name = '$name',
lname = '$lname',
nickname = '$nickname',
tel = '$tel',
address = '$address',
sub_district = '$sub_district',
district = '$district',
province = '$province',
postcode = '$postcode'
WHERE 
id_member ='$id'
";

$result = mysqli_query($conn, $updatemember);

if($result){
    echo json_encode(array("status" => "updatemember ok"));
}else{
    echo json_encode(array("status" => "updatemember error"));
    
}
// echo json_encode($password); // แสดงข้อมูล name เพื่อทดสอบ
?>
