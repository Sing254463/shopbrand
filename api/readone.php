<?php
require_once("../config/database.php");
header('Access-Control-Allow-Origin: *');
//  เพื่ออนุญาตให้เว็บไซต์อื่นสามารถเข้าถึงข้อมูลผ่าน API ของคุณ โดย * หมายถึงทุกๆ โดเมน
header("Content-type: application/json; charset=utf-8");
// ระบุว่าข้อมูลที่ส่งกลับจะเป็นประเภท JSON และใช้การเข้ารหัสเป็น UTF-8 เพื่อให้สามารถอ่านข้อมูลภาษาอื่นๆ ที่มีตัวอักษรพิเศษได้ถูกต้อง

$id = mysqli_real_escape_string($conn, $_GET['id']);

$array_users = ("SELECT * FROM member WHERE id_member = $id");
$check = mysqli_query($conn, $array_users);
$member = array();
foreach($check as $row){
    // print_r($row);
    array_push($member, array(
        'id_member' => $row['id_member'],
        'username' => $row['username'],
        'password' => $row['password'],
        'name' => $row['name'],
        'lname' => $row['lname'],
        'nickname' => $row['nickname'],
        'tel' => $row['tel'],
        'address' => $row['address'],
        'sub_district' => $row['sub_district'],
        'district' => $row['district'],
        'province' => $row['province'],
        'postcode' => $row['postcode'],
        'row' => $row['row']
    ));
}
echo json_encode($member)

?>