<?php
require_once("../../config/database.php");
header('Access-Control-Allow-Origin: *');
//  เพื่ออนุญาตให้เว็บไซต์อื่นสามารถเข้าถึงข้อมูลผ่าน API ของคุณ โดย * หมายถึงทุกๆ โดเมน
header("Content-type: application/json; charset=utf-8");
// ระบุว่าข้อมูลที่ส่งกลับจะเป็นประเภท JSON และใช้การเข้ารหัสเป็น UTF-8 เพื่อให้สามารถอ่านข้อมูลภาษาอื่นๆ ที่มีตัวอักษรพิเศษได้ถูกต้อง

$id = mysqli_real_escape_string($conn, $_GET['id']);

$array_users = ("SELECT * FROM products as pd INNER JOIN brand as bra ON pd.brand = bra.id
WHERE idproduct = $id");
$check = mysqli_query($conn, $array_users);
$member = array();
foreach($check as $row){
    // print_r($row);
    array_push($member, array(
        'idproduct' => $row['idproduct'],
        'nameproduct' => $row['nameproduct'],
        'description' => $row['description'],
        'brand' => $row['namebrand'],
        'id_brand' => $row['brand'],
        'price' => $row['price'],
        'quantity' => $row['quantity'],
        'img_profile' => $row['img_profile']
    ));
}
echo json_encode($member)
?>