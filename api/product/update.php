<?php
require_once("../../config/database.php");

header('Access-Control-Allow-Origin: *');
header("Content-type: application/json; charset=utf-8");
$data = json_decode(file_get_contents("php://input"));

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $nameproduct = $_POST['nameproduct'];
    $Product_details = $_POST['Product_details'];
    $price = $_POST['price'];
    $brand = $_POST['brand'];
    $quantity = $_POST['quantity'];
    $id = $_POST['id'];
    if (isset($_FILES['imageFile'])) {
        $file = $_FILES['imageFile'];
        // ตรวจสอบว่ามีข้อผิดพลาดในการอัพโหลดไฟล์หรือไม่
        if ($file['error'] === UPLOAD_ERR_OK) {
            // ย้ายไฟล์ไปยังโฟลเดอร์ที่ต้องการเก็บ
            $targetDir = "../../backend/imgproduct/"; // โฟลเดอร์ที่เก็บไฟล์
            $fileName = basename($file['name']);
            $targetFilePath = $targetDir . $fileName;

            // ย้ายไฟล์ไปยังโฟลเดอร์เก็บ
            if (move_uploaded_file($file['tmp_name'], $targetFilePath)) {

                $query = "UPDATE products SET
                nameproduct = '$nameproduct',
                 description = '$Product_details', 
                 brand = '$brand', 
                 price = '$price', 
                 quantity =  '$quantity', 
                 img_profile = '$fileName'
                 WHERE
                 idproduct = $id";

                    $sql_query = mysqli_query($conn, $query);
                    if($sql_query){
                        echo '{"status":"success"}';
                    }else{
                        echo '{"status":"error"}';
                    }

                    
                
            } else {
                echo json_encode(array("status" => "error_upload"));
            }
        } else {
            echo json_encode(array("status" => "error_upload"));
        }
    } else {
        $fileName = $_POST['imageFile'];
        $query = "UPDATE products SET
        nameproduct = '$nameproduct',
         description = '$Product_details', 
         brand = '$brand', 
         price = '$price', 
         quantity =  '$quantity', 
         img_profile = '$fileName'
         WHERE
         idproduct = $id";
         $sql_query = mysqli_query($conn, $query);
         if($sql_query){
             echo '{"status":"success"}';
         }else{
             echo '{"status":"error"}';
         }

    }
} else {
    // รับเฉพาะคำขอแบบ POST เท่านั้น
    echo '{"status":"error"}';
}
