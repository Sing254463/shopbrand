<?php
// config เป็นการ require ไฟล์ config.php มาใช้กับไฟล์ นี้
require_once("config/config.php");
//  database เป็นการ require ไฟล์ database.php มาใช้กับไฟล์ นี้
require_once("config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head เป็นการ require ไฟล์ head.php มาใช้กับไฟล์ นี้-->
    <?php require_once("template/head.php"); ?>
</head>

<body>
    <!-- navber เป็นการ require ไฟล์ navber.php มาใช้กับไฟล์ นี้-->
    <?php require_once("template/navber.php"); ?>

    <!-- ---------------------------------------- Carousel bootstrap ------------------------ -->

    <div id="carouselExample1" class="carousel slide">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://wallpaperswide.com/download/nike_9-wallpaper-1920x600.jpg" class="d-block mx-auto" alt=" " style="width: 100%;">
            </div>
            <div class="carousel-item">
                <img src="https://f.fcdn.app/imgs/d7b66e/menpi.uy/menpuy/4b42/webp/recursos/1221/1920x600/adidas-banner-1920x600.jpg" class="d-block mx-auto" alt=" " style="width: 100%;">
            </div>
            <div class="carousel-item">
                <img src="https://www.beckershoes.com/media/wysiwyg/nb.jpg" class="d-block" alt=" " style="width: 100%;">
            </div>
            <div class="carousel-item">
                <img src="https://www.elingenio.es/wp-content/uploads/2018/09/cabecera-hym.jpg" class="d-block" alt=" " style="width: 100%;">
            </div>
            <div class="carousel-item">
                <img src="img/Lee.png" class="d-block" alt=" " style="width: 100%;">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample1" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample1" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <!-- ---------------------------------------- Carousel bootstrap ------------------------ -->


<!-- --------------------------------------  container  -------------------------------------- -->
    <div class="container-fluid my-5">

        <!-- แบรนด์ที่รวมรายการ  -->
        <div class="container text-center my-5">
            <h1>แบรนด์ที่รวมรายการ</h1>
        </div>
        <!-- แบรนด์ที่รวมรายการ  -->


<!-- --------------------------------------  Carousel bootstrap slide -------------------------------------- -->

        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row row-cols-4 row-cols-sm-4 row-cols-md-4 g-4">

                    <!-- เป็นการกำหนด class row เอาไว้ โดยมรการ จัดให้ d-flex justify-content-center และ mx-auto หรือให้อยู่ตรงกลาง 
                         gap-5 เป็นการ จัดระยะ ห่าง สูงสุด คือ 6 คือ ชิด
                -->
                <!-- <div class="container"> -->
                        <div class="col d-flex justify-content-center">
                            <a href="#">
                                <!-- เป็นการกำหนด ลิ้งให้ปลายทางเมื่อทำการกดจะไปหน้าเว็บนั้นหรือไฟล์นั้น -->
                                <div class="card Brand">
                                <!-- card bootstrap -->
                                    <img src="https://crockerpark.com/wp-content/uploads/hm-logo.png" class="card-img-top" alt="...">
                                    <!-- การใส่รูป -->
                                </div>
                            </a>
                        </div>

                        <!-- เป็นการกำหนด clss col-sm-2 และ จัดให้อยู่ ตรงกลาง d-flex justify-content-center 
                             col นั้น ความยาวนั้น เป็น 12 โดนสามารถ เป็นเป็น บลอก ได้ 12 บลอก โดย ถ้า ทำการ กำหนด 3 บลอก จะทำการ เอา 3/12 ก็จะเท่ากับ 4
                             แต่อันนี้ ทำการกดหนด คือ 2 สามารถสร้างบลอกได้ 6 บลอก แต่ทำการกำหนด คือ 3
                        -->
                        

                        <div class="col  d-flex justify-content-center ">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpM8UWG-4Cd9v6bvl1FT0ErHuQfXOU-8McCcVhC9aDYg&s" class="card-img-top" alt="...">

                                </div>
                            </a>
                        </div>
                        <div class="col  d-flex justify-content-center">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://down-th.img.susercontent.com/file/6da657858c18d396c7f9fa22d53680b5" class="card-img-top" alt="...">

                                </div>
                            </a>
                        </div>
                        <div class="col  d-flex justify-content-center">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJp-JO8arvFt_xQRoQPvFgLqU6S0C-2xllWTIR0i8MbA&s" class="card-img-top" alt="...">
                                </div>
                            </a>
                        <!-- </div> -->
                    </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row row-cols-4 row-cols-sm-4 row-cols-md-4 g-4">
                        <div class="col-2  d-flex justify-content-center">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://img.salehere.co.th/p/1200x0/2020/12/29/zzj4my0gz9p9.jpg" class="card-img-top" alt="...">

                                </div>
                            </a>
                        </div>
                        <div class="col-2  d-flex justify-content-center">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fsubstack-post-media.s3.amazonaws.com%2Fpublic%2Fimages%2F5362a828-0f5b-4d17-a6c5-d0677dc89baa_1000x1000.jpeg" class="card-img-top" alt="...">
                                </div>
                            </a>
                        </div>
                        <div class="col-2  d-flex justify-content-center">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://i.pinimg.com/564x/73/ba/b5/73bab5dcd93e8cf099e4d3aa84b4183b.jpg" class="card-img-top" alt="...">
                                </div>
                            </a>
                        </div>
                        <div class="col-2  d-flex justify-content-center">
                            <a href="#">
                                <div class="card Brand">
                                    <img src="https://i.pinimg.com/564x/9e/b9/8d/9eb98d5f1773b255a99cf2a6b43a306e.jpg" class="card-img-top" alt="...">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev btn-Brand" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev" aria-hidden="true" style="color: black;"><i class="bi bi-chevron-left" style="font-size: 30px; color: black;"></i></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next btn-Brand" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next" aria-hidden="true" style="color: black;"> <i class="bi bi-chevron-right" style="font-size: 30px; color: black;"></i></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>





    </div>
<!-- --------------------------------------  container  -------------------------------------- -->



    <!-- footer เป็นการ require ไฟล์ footer.php มาใช้กับไฟล์ นี้-->
    <?php require_once("template/footer.php"); ?>

   <!-- script jquery 3.7.1 -->
   <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

<!-- script bootstrap 5.3.3 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<!--script datatables 2.0.2 -->
<script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

<!--script popperjs 2.0.2 -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

<!-- sweetalert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>