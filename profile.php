<?php
// config
require_once("config/config.php");
//  database
require_once("config/database.php");
?>
<?php
require_once("config/config.php");
require_once("config/database.php");

$id = $_SESSION['id_member'];

$sql = "SELECT * FROM member WHERE id_member = '$id'";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_assoc($result);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>
</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>


    <div class="container my-3 p-3 d-flex justify-content-center">
        <div class="card" style="width: 45rem;">
            <div class="card-header text-center">

                <h1>ข้อมูลของคุณ</h1>
            </div>
            <div class="card-body">
                <div class="mb-3 row">
                    <label for="username" class="col-sm-2 col-form-label">Username :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="username" value="<?= $row['username']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="name" class="col-sm-2 col-form-label">name :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="name" value="<?= $row['name']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="lname" class="col-sm-2 col-form-label">lname :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="lname" value="<?= $row['lname']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="nickname" class="col-sm-2 col-form-label">nickname :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="nickname" value="<?= $row['nickname']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="tel" class="col-sm-2 col-form-label">tel :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="tel" value="<?= $row['tel']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="address" class="col-sm-2 col-form-label">ที่อยู่ :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="address" value="<?= $row['address']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="sub_district" class="col-sm-2 col-form-label">ตําบล/แขวง :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="sub_district" value="<?= $row['sub_district']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="district" class="col-sm-2 col-form-label">เขต/อําเภอ :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="district" value="<?= $row['district']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="province" class="col-sm-2 col-form-label">จังหวัด :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="province" value="<?= $row['province']; ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="postcode" class="col-sm-2 col-form-label">รหัสไปสณี :</label>
                    <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="postcode" value="<?= $row['postcode']; ?>">
                    </div>
                </div>
            </div>
            <div class="container text-center my-3">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editprofileModal">
                    แก้ไขข้อมูล
                </button>
            </div>
        </div>
    </div>
    <!-- แก้ไขข้อมูล -->
    <div class="modal fade" id="editprofileModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label  class="form-label">username</label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label">Password</label>
                        <input type="password" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label">Password</label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label"></label>
                        <input type="text" class="form-control" id="">
                    </div>
                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                    <button type="button" class="btn btn-primary">แก้ไขข้อมูล</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

   <!-- script jquery 3.7.1 -->
   <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

<!-- script bootstrap 5.3.3 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<!--script datatables 2.0.2 -->
<script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

<!--script popperjs 2.0.2 -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

<!-- sweetalert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>