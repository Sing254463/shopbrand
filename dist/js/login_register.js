// ส่วนของ login 
function login() {
    var login = true;
    // เป็นการตั่งค่า ให้ตัวแปล login = true
    if ($('#username').val().length <= 0) {
        // ทำการเช็ค id="username" มี ความยาว น้อยกว่าหรือเท่ากับ 0 หรือไม
        // ถ้าน้อยดว่าหรือเท่ากับ 0 จะให้ทำขั้นตอน ข้างในคือ
        login = false;
        // เปลียน ค่า ของ login เป็น false
        Swal.fire({
            title: "ไม่สามารถเข้าสู่ระบบได้",
            text: "กรุณาทำการกรอก ชื่อผู้ใช้",
            showConfirmButton: false,
            icon: "error"
        });
        // ให้มีการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า ไม่สามารถเข้าสู่ระบบได้  กรุณาทำการกรอก ชื่อผู้ใช้
    } else if($('#password').val().length <= 0){
        // ทำการเช็ค id="password" มี ความยาว น้อยกว่าหรือเท่ากับ 0 หรือไม
        // ถ้าน้อยดว่าหรือเท่ากับ 0 จะให้ทำขั้นตอน ข้างในคือ
        login = false;
        // เปลียน ค่า ของ login เป็น false
        Swal.fire({
            title: "ไม่สามารถเข้าสู่ระบบได้",
            text: "กรุณาทำการกรอก รหัสผ่าน",
            showConfirmButton: false,
            icon: "error"
            
        });
        // ให้มีการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า ไม่สามารถเข้าสู่ระบบได้  กรุณาทำการกรอก รหัสผ่าน
    }
    if (login) {
        // ทำการเช็ค ว่า login มีค่าเท่ากับ true หรือไม
        // console.log($('#username').val()); เป็นการเช็ค ว่า#username มีค่าอะไร จะโชใน console ของgoogle
        $.ajax({
            // ทำการเขียน ajax
            method:'post',
            // กำหนด method = POST
            url:'system/login_register.php',
            // ทำการเลือกเส้นทาง ไปยัง system/login_register.php
            data:{
                username: $('#username').val(),
                password: $('#password').val(),
                btn_login: true
            },
            // การกำหนดข้อมูล โดยมี การสร้าง ตัวแปล คือ
            // username ให้เก็บ ค่าข้อมูล คือ $('#username').val() หรือข้อมูล ของ id="username"
            // password ให้เก็บ ค่าข้อมูล คือ $('#password').val() หรือข้อมูล ของ id="password"
            // btn_loginให้ ค้าข้อมูลเป็น true
            success:function(status){
                // เมื่อทำการส่งข้อมูล สำเร็จ จะทำการ รับข้อมูลกลับมา โดยรับ ข้อมูลที่ส่งเข้ามาแล้วเอาไปใส่ใน พารามิเตอร์ ชื่อ status เข้ามา
                var error = JSON.parse(status)
                // สร้างตัวแปล error แล้วทำการ เอา พารามิเตอร์ ชื่อ status ที่เป็น json มาแปลง เป็น object
                if(error.status === '404'){
                    // ทำการเช็ค ว่า stratus ใน object นั้นมีค่า เป็น '404' หรือไม
                    // ถ้าใช้ จะทำการ ขั้นตอนข้างใน if
                    Swal.fire({
            title: "ไม่สามารถเข้าสู่ระบบได้",
            text: "ชื่อผู้ใช้ หรือ รหัสผ่าน ของท่านไม่ถูกต้อง",
            showConfirmButton: false,
            icon: "error"
        });
        // ให้มีการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า ไม่สามารถเข้าสู่ระบบได้  ชื่อผู้ใช้ หรือ รหัสผ่าน ของท่านไม่ถูกต้อง
                }else{
                    // ถ้าเงื่อนไขไม่ถูกต้อง จะทำขั้นตอน ข้างใน ของ else
                    Swal.fire({
            title: "เข้าสู่ระบบสำเร็จ",
            text: "ยินดีต้อนรับ",
            showConfirmButton: true,
            icon: "success",
            // timer: 800
        })
        // // ให้มีการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า ไม่สามารถเข้าสู่ระบบได้  เข้าสู่ระบบสำเร็จ ยินดีต้อนรับ
        .then(()=>{
        window.location.href = 'index.php';})
                }
            }
            // .then คือ เมื่อการทำงานของ sweetalert2 จบ ลง จะทำงาน ระบบ window.location.href = 'index.php'; ต่อ
            // เป็นการ รอ ให้ คำสั่ง ของข้างบน สำเร็จก่อน ที่จะทำ คำสั่ง ต่อไป ที่อยู่ ข้างใน
        })
    }
}
// -----------------------------------------------------------------------
// ส่วนของ register 
function register(){
    // ทำการสร้าง ฟังชั้นชื่อ register
    var Register = true;
    // กำหนด Register ให้เท่ากับ true
    if($('#password1').val().length <= 0 && $('#password2').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ password",
            icon: "error",
        });
        // ทำการเช็ค ว่า ถ้า id password1 และ password2 นั้น มีความยาว น้อยกว่าหรือเท่ากับ 0
        // จะทำการให้ Register = false;
        // แล้วทำการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า สมัครสมาชิกไม่สำเร็จ กรุณาใส่ password
    }
    else if ($('#password1').val() != $('#password2').val()){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "รหัสผ่านของคุณไม่ตรงกัน",
            icon: "error",
        });
        // ทำการเช็ค ว่า ถ้า id password1 ไม่เท่ากับ password2
        // จะทำการให้ Register = false;
        // แล้วทำการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า สมัครสมาชิกไม่สำเร็จ รหัสผ่านของคุณไม่ตรงกัน
    }
    else if($('#register_username').val().length <= 0){
        Register = false;
        Swal.fire({
            title: "สมัครสมาชิกไม่สำเร็จ",
            text: "กรุณาใส่ ชื่อผู้ใช้",
            icon: "error",
        });
        // ทำการเช็ค ว่า ถ้า id register_username นั้น มีความยาว น้อยกว่าหรือเท่ากับ 0
        // จะทำการให้ Register = false;
        // แล้วทำการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า สมัครสมาชิกไม่สำเร็จ กรุณาใส่ ชื่อผู้ใช้
    }
    
    
    if(Register){
        // ทำการเช็ค ว่า Register นั้นเท่ากับ true หรือไม
        $.ajax({
            // ทำการเรียกใช้ ajax
            method: 'post',
            // กำหนด method เป็น post
            url: 'system/login_register.php',
            // กำหนด url หรือปลายทางไปยัง system/login_register.php
            data: {
                // data ทำการกำหนดข้อมูล
                register_username: $('#register_username').val(),
                password1: $('#password1').val(),
                password2: $('#password2').val(),
                btn_register: true
                // การกำหนดข้อมูล โดยมี การสร้าง ตัวแปล คือ
            // register_username ให้เก็บ ค่าข้อมูล คือ $('#register_username').val() หรือข้อมูล ของ id="register_username"
            // password1 ให้เก็บ ค่าข้อมูล คือ $('#password1').val() หรือข้อมูล ของ id="password1"
            // password2 ให้เก็บ ค่าข้อมูล คือ $('#password2').val() หรือข้อมูล ของ id="password2"
            // btn_register ค้าข้อมูลเป็น true
            },
            success:function(status){
                // success ทำการกำหนด function เก็บพารามิเตอ status
                var error = JSON.parse(status)
                // ทำการแปลง JSON เป็น object
                if(error.status === 'duplicate_username'){
                    Swal.fire({
                        title: "สมัครสมาชิกไม่สำเร็จ",
                        text: "มี username นี้แล้ว",
                        icon: "error",
                    });
                    // ทำการเช็ค ว่า error ค่า status นั้น เท่ากับ  duplicate_username
                    // แล้วทำการ แจ้งเตื่อน หรือ alert ของ sweetalert2
        // บอกว่า สมัครสมาชิกไม่สำเร็จ มี username นี้แล้ว
                }else if(error.status === 'password_mismatch'){
                    Swal.fire({
                        title: "สมัครสมาชิกไม่สำเร็จ",
                        text: "รหัสผ่านของคุณไม่ตรงกัน",
                        icon: "error",
                    });
                    // ทำการเช็ค ว่า error ค่า status นั้น เท่ากับ  password_mismatch
                    // แล้วทำการ แจ้งเตื่อน หรือ alert ของ sweetalert2
                    // บอกว่า สมัครสมาชิกไม่สำเร็จ รหัสผ่านของคุณไม่ตรงกัน
                }else{
                $('#register_username').val(""),
                $('#password1').val(""),
                $('#password2').val(""),
                    Swal.fire({
                        title: "สมัครสมาชิกสำเร็จ",
                        text: "",
                        icon: "success"
                    });
                    // ถ้าไม่ตรงกับเงื่อนไขอะไรเลย จะทำการ ให้ ค่าข้อมูล ของ id register_username password1 password2
                    // นั้นเท่ากับค่าว่าง
                    // แล้วทำการ แจ้งเตื่อน หรือ alert ของ sweetalert2
                    // บอกว่า สมัครสมาชิกสำเร็จ
                }
            }
        })
    }
}