<?php
// config
require_once("config/config.php");
//  database
require_once("config/database.php");
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- head -->
    <?php require_once("template/head.php"); ?>
</head>

<body>
    <!-- navber -->
    <?php require_once("template/navber.php"); ?>
    <div class="container my-5">
    <div class="container my-5">
    <div class="row d-flex">
        <?php
        $showproduct = "SELECT * FROM products as pd INNER JOIN brand as br ON pd.brand = br.id ORDER BY pd.brand";
        $chack = mysqli_query($conn, $showproduct);

        // Initialize $currentBrand to null
        $currentBrand = null;

        foreach ($chack as $pro) {
            // Check if brand has changed
            if ($pro['brand'] !== $currentBrand) {
                // Display brand name when it changes
                echo '<div class="w-100 my-3"><h3>ชื่อแบรนด์: ' . $pro["namebrand"] . '</h3></div>';
                $currentBrand = $pro['brand'];
            }

            // Display product card
            echo '<div class="col-sm-4 d-flex justify-content-center my-2">';
            echo '<a href="product.php?id='.$pro["idproduct"].'" class="link-offset-2 link-underline link-underline-opacity-0 text-dark">';
            echo '<div class="card" style="width: 20rem;">';
            echo '<img src="backend/imgproduct/' . $pro["img_profile"] . '" class="card-img-top" alt="รูปสินค้า">';
            echo '<div class="card-body text-center">';
            echo '<h5 class="card-title">' . $pro["nameproduct"] . '</h5>';
            echo '<p class="card-text">' . $pro["description"] . '</p>';
            echo '<p class="text-start">ราคา: <b style="font-size: 20px;"><i class="bi bi-currency-bitcoin"></i>' . $pro["price"] . '</b></p>';
            echo '</div>';
            echo '</div>';
            echo '</a>';
            echo '</div>';
        }
        ?>
    </div>
</div>


    <!-- footer -->
    <?php require_once("template/footer.php"); ?>

   <!-- script jquery 3.7.1 -->
   <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

<!-- script bootstrap 5.3.3 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<!--script datatables 2.0.2 -->
<script src="https://cdn.datatables.net/2.0.2/js/dataTables.min.js"></script>

<!--script popperjs 2.0.2 -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>

<!-- sweetalert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>